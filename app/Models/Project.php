<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Project extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $fillable = ['title', 'description', 'manager_id', 'client_id', 'due_date', 'status_id'];

     protected $hidden = ['deleted_at'];

    protected $casts = [
        'due_date' => 'datetime:d-m-Y'
    ];


    public static function booted()
    {
        static::deleting(function (Project $project) {
            $project->status_id = 4; //Closed
            $project->manager_id = null;
            $project->save();
            $project->tasks()->delete();
        });
    }

    public function scopeFilter($query, array $filters): Void
    {
        $query->when($filters['status'] ?? false, function($query, $status) {
            return $query->whereHas('status', function($query) use ($status) {
                $query->where('slug', $status);
            })->get();
        });
    }

    public function scopeActiveProjects($query)
    {
        return $query->where('status_id', 1)->orWhere('status_id', 2)->get(['id', 'title']);
    }

    public function manager(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class)->withTrashed();
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class);
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['title', 'description', 'manager.name', 'client.company_name', 'due_date', 'status.name'])
            ->logOnlyDirty();
    }
}
