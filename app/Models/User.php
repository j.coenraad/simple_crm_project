<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, HasRoles, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'token',
        'password_changed_at',
        'new_token_requested_at',
        'token_reset_at',
        'token_expires_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'password_changed_at',
        'deleted_at',
        'email_verified_at',
        'new_token_requested_at',
        'token_reset_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password_changed_at' => 'datetime',
        'new_token_requested_at' => 'datetime',
        'token_expires_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting( function(User $user) {
            $user->tasks()->each(function ($task) {
                $task->user_id = null;
                $task->save();
            });

            $user->projects()->each(function ($project) {
                $project->manager_id = null;
                $project->save();
            });
        });
    }

    public function projects(): HasMany
    {
        return $this->hasMany(Project::class, 'manager_id');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class)->latest();
    }

    public function getIsTokenExpiredAttribute(): bool
    {
        return !empty($this->token_expires_at) && $this->token_expires_at < now();
    }

    public function getHasRequestedNewTokenAttribute(): bool
    {
        return $this->new_token_requested_at != null;
    }
}
