<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->company_name,
            'address' => $this->company_address,
            'postcode' => $this->company_zip,
            'city' => $this->company_city,
            'country' => $this->company_country,
            'contact' => [
                'name' => $this->contact_name,
                'email' => $this->contact_email,
                'phone' => $this->contact_phone
            ],
            'projects' => ProjectResource::collection($this->whenLoaded('projects')),
        ];
    }
}
