<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AssignTaskController extends Controller
{
    public function __invoke(Task $task): RedirectResponse
    {
        $task->update(['user_id' => Auth::id()]);

        return redirect()->route('tasks.show', $task)
            ->with('success', 'You have assigned the task to yourself.');
    }
}
