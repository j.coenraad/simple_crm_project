<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UnassignTaskController extends Controller
{
    public function __invoke(Task $task): RedirectResponse
    {
        if (!$task->user_id) {
            return redirect()->route('tasks.show', $task)->withErrors(['error' => 'The task has already been unassigned.']);
        }

        abort_if(Auth::id() != $task->user_id && auth()->user()->cannot('unassign_task'), 403, 'This action is unauthorized.');

        $task->update([
            'user_id' => null
        ]);

        return redirect()->route('tasks.show', $task)
            ->with('success', 'The task has been unassigned.');
    }
}
