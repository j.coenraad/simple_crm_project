<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function __invoke(): View
    {
        $user = auth()->user();
        $tasks = Task::where('user_id', $user->id)->with('project')->paginate(10);

        return view('dashboard', compact('user','tasks'));
    }
}
