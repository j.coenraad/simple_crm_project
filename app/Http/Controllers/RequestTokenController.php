<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewTokenRequest;
use App\Models\User;
use App\Notifications\NewTokenRequested;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class RequestTokenController extends Controller
{
    public function create(Request $request)
    {
        $token = $request->route()->parameter('token');
        $user = User::where('token', $token)->first();

        if (!empty($user->password_changed_at)) {
            return redirect()->route('change-password.create', $user->token)->withErrors('User has already activated the account.');
        }

        if (!$user->is_token_expired) {
            return redirect()->route('change-password.create', $user->token)->withErrors(['error' => 'The token has not yet expired.']);
        }

        if ($user->has_requested_new_token) {
            return redirect()->route('change-password.create', $user->token)->withErrors(['error' => 'A new token has already been requested.']);
        }

        return view('request-token.create', compact('token'));
    }

    public function store(NewTokenRequest $request): RedirectResponse
    {
        $user = User::where(['token' => $request->token, 'email' => $request->email])->first();

        if (!empty($user->password_changed_at)) {
            return redirect()->route('change-password.create', $user->token)->withErrors('User has already activated the account.');
        }

        if (!$user->is_token_expired) {
            return redirect()->route('change-password.create', $user->token)->withErrors('The token has not yet expired.');
        }

        if ($user->has_requested_new_token) {
            return redirect()->route('change-password.create', $user->token)->withErrors(['error' => 'A new token has already been requested.']);
        }

        $user->update([
            'new_token_requested_at' => now()
        ]);

        $admins = User::role('admin')->get();
        Notification::send($admins, new NewTokenRequested($user));

        return redirect()->route('change-password.create', $user->token)
            ->with('success', 'A new token has been requested.');
    }
}
