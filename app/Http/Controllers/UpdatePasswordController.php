<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePasswordRequest;
use App\Mail\PasswordChangedMail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;

class UpdatePasswordController extends Controller
{
    public function store(UpdatePasswordRequest $request): RedirectResponse
    {
        abort_if(auth()->user() != $request->user(), 403, 'User is not authorized to perform this action.');

        $request->user()->update([
            'password' => bcrypt($request->password)
        ]);

        Mail::to($request->user()->email)->queue(new PasswordChangedMail($request->user()->name));

        return redirect()->route('profiles.show', $request->user())
            ->with('success', 'Your password has been changed.');
    }
}
