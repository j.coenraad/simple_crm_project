<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\View\View;

class TrashedUserController extends Controller
{
    public function __invoke(): View
    {
        if(auth()->user()->cannot('read_trashed')) {
            abort(403, 'User does not have the right permissions.');
        }

        $users = User::onlyTrashed()
            ->orderByDesc('deleted_at')
            ->paginate(10);

        return view('users.trashed', compact('users'));
    }
}
