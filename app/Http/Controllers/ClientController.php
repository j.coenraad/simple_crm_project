<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Models\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;


class ClientController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Client::class, 'client');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $builder = Client::query();

        if ($search = request()->get('search')) {
            $builder->where('company_name', 'LIKE', '%'.$search.'%')
                ->orWhere('contact_name', 'LIKE', '%'.$search.'%');
        }

        $clients = $builder->paginate(10);

        return view('clients.index', compact('clients', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientRequest $request
     * @return RedirectResponse
     */
    public function store(ClientRequest $request): RedirectResponse
    {
        Client::create($request->validated());

        return redirect()->route('clients.index')
            ->with('success', 'The client has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return View
     */
    public function show(Client $client): View
    {
        $projects = $client->projects()->with('manager', 'status')->paginate(10);

        return view('clients.show', compact('client', 'projects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @return View
     */
    public function edit(Client $client): View
    {
        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientRequest $request
     * @param Client $client
     * @return RedirectResponse
     */
    public function update(ClientRequest $request, Client $client): RedirectResponse
    {
        $client->update($request->validated());

        return redirect()->route('clients.show', $client)
            ->with('success', 'The client has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @return RedirectResponse
     */
    public function destroy(Client $client): RedirectResponse
    {
        $client->delete();

        return redirect()->route('clients.index')
            ->with('success', 'The client has been deleted.');
    }

    public function restore(Client $client): RedirectResponse
    {
        $this->authorize('restore', $client);

        $client->restore();

        return redirect()->route('clients.trashed')
            ->with('success', 'The client has been restored!');
    }

    public function forceDelete(Client $client): RedirectResponse
    {
        $this->authorize('forceDelete', $client);

        $client->forceDelete();
        return redirect()->route('clients.trashed')
            ->with('success', 'The client has been permanently deleted.');
    }
}
