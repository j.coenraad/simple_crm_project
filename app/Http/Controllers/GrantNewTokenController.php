<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\NewTokenGranted;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GrantNewTokenController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function __invoke(Request $request, User $user): RedirectResponse
    {
        if ($user->password_changed_at) {
            return redirect()->route('users.show', $user)->withErrors(['error' => 'User has already activated their account']);
        }

        if ($user->token_expires_at > now()) {
            return redirect()->route('users.show', $user)->withErrors(['error' => 'The token of the user has not expired yet']);
        }

        if (empty($user->new_token_requested_at)) {
            return redirect()->route('users.show', $user)->withErrors(['error' => 'This user has not requested a new token.']);
        }

        $user->update([
            'token' => Str::random(40),
            'token_expires_at' => now()->addDays(3),
            'token_reset_at' => now(),
            'new_token_requested_at' => null,
        ]);

        $user->notify(new NewTokenGranted($user));

        return redirect()->route('users.show', $user)
            ->with('success', 'A new token has been created.');
    }
}
