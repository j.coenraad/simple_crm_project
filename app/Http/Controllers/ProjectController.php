<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Models\Client;
use App\Models\Project;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Project::class, 'project');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        // manager is user relation
        $builder = Project::query();

        if ($search = request()->get('search')) {
            $builder->where('title', 'LIKE', '%'.$search.'%');
        }

        $projects = $builder->latest()
            ->filter(request()->only('status'))
            ->with('client', 'manager', 'status')
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        return view('projects.index', compact('projects', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $clients = Client::all();
        $managers = User::role(['admin', 'manager'])->get();

        return view('projects.create', compact('clients', 'managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequest $request
     * @return RedirectResponse
     */
    public function store(ProjectRequest $request): RedirectResponse
    {
        $request->user()->projects()->create($request->validated());

        return redirect()->route('projects.index')
            ->with('success', 'The project has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return View
     */
    public function show(Project $project): View
    {
        $tasks = $project->tasks()
            ->with('user', 'status')
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        return view('projects.show', compact('project', 'tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return View
     */
    public function edit(Project $project): View
    {
        $clients = Client::all();
        $managers = User::role(['admin', 'manager'])->get();
        $states = Status::all();

        return view('projects.edit', compact('project', 'clients', 'managers', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectRequest $request
     * @param Project $project
     * @return RedirectResponse
     */
    public function update(ProjectRequest $request, Project $project): RedirectResponse
    {
        $project->update($request->validated());

        return redirect()->route('projects.show', $project)
            ->with('success', 'The project has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return RedirectResponse
     */
    public function destroy(Project $project): RedirectResponse
    {
        $project->delete();

        return redirect()->route('projects.index')
            ->with('success', 'The project has been deleted.');
    }

    public function restore(Project $project): RedirectResponse
    {
        $this->authorize('restore', $project);

        if ($project->client->trashed()) {
            return redirect()->route('projects.trashed')
                ->with('danger', 'Could not restore the project. The client has been deleted. You need to restore the client before you can restore the project!');
        }

        $project->restore();

        return redirect()->route('projects.trashed')
            ->with('success', 'The project has been restored!');
    }

    public function forceDelete(Project $project): RedirectResponse
    {
        $this->authorize('forceDelete', $project);

        $project->forceDelete();

        return redirect()->route('projects.trashed')
            ->with('success', 'The project has been permanently deleted.');
    }
}
