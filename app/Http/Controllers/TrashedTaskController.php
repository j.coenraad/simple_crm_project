<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Response;
use Illuminate\View\View;

class TrashedTaskController extends Controller
{
    public function __invoke(): View
    {
        if(auth()->user()->cannot('read_trashed')) {
            abort(403, 'User does not have the right permissions.');
        }

        $tasks = Task::onlyTrashed()
            ->with('project', 'user', 'status')
            ->orderBy('deleted_at', 'desc')
            ->paginate(10);

        return view('tasks.trashed', compact('tasks'));
    }
}
