<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\View\View;

class TrashedClientController extends Controller
{
    public function __invoke(): View
    {
        if(auth()->user()->cannot('read_trashed')) {
            abort(403, 'User does not have the right permissions.');
        }

        $clients = Client::onlyTrashed()
            ->orderByDesc('deleted_at')
            ->paginate(10);

        return view('clients.trashed', compact('clients'));
    }
}
