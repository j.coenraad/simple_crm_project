<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProfileController extends Controller
{
    public function show(User $user): View
    {
        return view('profiles.show', compact('user'));
    }

    public function edit(User $user): View
    {
        abort_if($user->id != Auth::id(), 403, 'A user can only edit their own profile.');

        return view('profiles.edit', compact('user'));
    }

    public function update(ProfileRequest $request, User $user): RedirectResponse
    {
        abort_if($user->id != Auth::id(), 403, 'A user can only edit their own profile.');

        $user->update($request->only('name'));

        if($request->has('image')) {
            if($user->getFirstMedia()) {
                $user->clearMediaCollection();
            }

            $user
                ->addMediaFromRequest('image')
                ->toMediaCollection();
        }

        return redirect()->route('profiles.show', $user)->with('success', 'Your profile has been updated.');
    }
}
