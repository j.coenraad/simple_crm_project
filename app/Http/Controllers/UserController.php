<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Mail\AccountCreated;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    public function index(): View
    {
        $builder = User::query();

        if ($search = request()->get('search')) {
           $builder->where('name', 'LIKE', '%'.$search.'%')
            ->orWhere('email', 'LIKE', '%'.$search.'%');
        }

        $users = $builder->with('roles')->paginate(10);

        return view('users.index', compact('users', 'search'));
    }

    public function create(): View
    {
        if (auth()->user()->hasRole('super-admin')) {
            $roles = Role::all();
        } else {
            $roles = Role::whereIn('name', ['employee', 'manager'])->get();
        }

        return view('users.create', compact('roles'));
    }

    public function store(UserRequest $request): RedirectResponse
    {
        $user = User::make($request->only(['name', 'email']));
        $user['password'] = Hash::make(Str::random(18));
        $user['token'] = Str::random(40);
        $user['token_expires_at'] = now()->addDays(3);
        $user->save();

        if($request->role_id == null) {
            $user->assignRole('employee');
        }

        $user->assignRole($request->role_id);

        Mail::to($user)->queue(new AccountCreated($user['token'], $user['name']));

        return redirect()->route('users.index')
            ->with('success', 'A new user account has been created.');
    }

    public function show(User $user): View
    {
        if ($user->is_token_expired) {
            dump($user->token_expires_at);
        }

        $user->load('roles');

        return view('users.show', compact('user'));
    }

    public function edit(User $user): View
    {
        if($user->hasRole('super-admin') && !auth()->user()->hasRole('super-admin')) {
            abort(403, 'You are not authorized to perform this action');
        }

        if(auth()->user()->hasRole('super-admin')) {
            $roles = Role::all();
        } else {
            $roles = Role::whereIn('name', ['manager', 'employee'])->get();
        }

        return view('users.edit', compact(['user', 'roles']));
    }

    public function update(UserRequest $request, User $user): RedirectResponse
    {
        $user->update($request->only(['name', 'email']));

        $user->syncRoles($request->role_id);

        return redirect()->route('users.index')
            ->with('success', 'The user account has been updated.');
    }

    public function destroy(User $user): RedirectResponse
    {
        if($user->hasRole('super-admin') && !auth()->user()->hasRole('super-admin')) {
            abort(403, 'You are not authorized to perform this action');
        }

        $user->delete();

        return redirect()->route('users.index')
            ->with('success','The user account has been deleted.');
    }

    public function restore(User $user): RedirectResponse
    {
        $this->authorize('restore', $user);

        $user->restore();

        return redirect()->route('users.trashed')
            ->with('success', 'The user has been restored.');
    }
}
