<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\View\View;

class TrashedProjectController extends Controller
{
    public function __invoke(): View
    {
        if(auth()->user()->cannot('read_trashed')) {
            abort(403, 'User does not have the right permissions.');
        }

        $projects = Project::onlyTrashed()
            ->with('status', 'client', 'manager')
            ->orderByDesc('deleted_at')
            ->paginate(10);

        return view('projects.trashed', compact('projects'));
    }
}
