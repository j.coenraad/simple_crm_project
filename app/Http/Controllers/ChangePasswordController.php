<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Mail\PasswordChangedMail;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class ChangePasswordController extends Controller
{
    public function create(Request $request): Mixed
    {
        $token = $request->route()->parameter('token');

        $user = User::where('token', $token)->first();

        if (!isset($user->token_expires_at) || $user->password_changed_at) {
            return redirect()->route('login');
        }

        $expires_at = $user->token_expires_at;

        $token_requested = $user->new_token_requested_at;

        if ($user->password_changed_at) {
            return redirect()->route('login');
        }

        return view('auth.change-password', compact(['token', 'expires_at', 'token_requested']));
    }

    public function store(ChangePasswordRequest $request): RedirectResponse
    {
        $user = User::where(['token' => $request->token, 'email' => $request->email])->first();

        if ($user->is_token_expired  && !$user->has_requested_new_token) {
            return redirect()->route('change-password.create', $user->token)
                ->withErrors(['error' => 'The token has expired. Please request a new token to activate your account.']);
        }

        if ($user->is_token_expired  && $user->has_requested_new_token) {
            return redirect()->route('change-password.create', $request->token)
                ->withErrors('The current token has expired. Please wait until a new token has been provided.');
        }

        $user->update([
            'password' => Hash::make($request->password),
            'token' => null,
            'token_expires_at' => null,
            'password_changed_at' => now()
        ]);

        Mail::to($user->email)->queue(new PasswordChangedMail($user));

        return redirect()->route('login')->with('success', 'The password has been changed!');
    }
}
