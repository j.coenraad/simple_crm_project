<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Mail\TaskAssignedMail;
use App\Models\Task;
use App\models\Project;
use App\Models\Status;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Task::class, 'task');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $builder = Task::query();

        if ($search = request()->get('search')) {
            $builder->where('id', $search)
                ->orwhere('name', 'LIKE', '%'.$search.'%');
        }

        $tasks = $builder->latest()
            ->filter(request()->only('status'))
            ->with(['project', 'user', 'status'])
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        return view('tasks.index', compact('tasks', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $projects = Project::activeProjects();
        $users = User::all('id', 'name');

        return view('tasks.create', compact(['projects', 'users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskRequest $request
     * @return RedirectResponse
     */
    public function store(TaskRequest $request): RedirectResponse
    {
        $task = Task::create($request->validated());

        if($request->user_id && Auth::id() != $request->user_id) {
            $user = User::find($request->user_id);

            Mail::to($user)->queue(new TaskAssignedMail($task));
        }

        return redirect()->route('tasks.index')
            ->with('success', 'The task has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param Task $task
     * @return View
     */
    public function show(Task $task): View
    {
        return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Task $task
     * @return View
     */
    public function edit(Task $task): View
    {
        $projects = Project::activeProjects();
        $users = User::all('id', 'name');
        $states = Status::all();

        return view('tasks.edit', compact(['task', 'projects', 'users', 'states']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskRequest $request
     * @param Task $task
     * @return RedirectResponse
     */
    public function update(TaskRequest $request, Task $task): RedirectResponse
    {
        $task->update($request->validated());

        if (!empty($request->user_id) && $request->user_id != Auth::id()) {
            $user = User::find($request->user_id);

            Mail::to($user)->queue(new TaskAssignedMail($task));
        }

        return redirect()->route('tasks.show', $task)
            ->with('success', 'The task has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return RedirectResponse
     */
    public function destroy(Task $task): RedirectResponse
    {
        $task->delete();

        return redirect()->route('tasks.index')
            ->with('success', 'The task has been deleted.');
    }

    /**
     * @param Task $task
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function restore(Task $task): RedirectResponse
    {
        $this->authorize('restore', $task);

        if ($task->project->trashed()) {
            return redirect()->route('tasks.trashed')->with('danger', 'Could not restore the task. The project has been deleted. To restore this task you need to restore the project first.');
        }

        $task->restore();

        return redirect()->route('tasks.trashed')
            ->with('success', 'The task has been restored!');
    }

    /**
     * @param Task $task
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function forceDelete(Task $task): RedirectResponse
    {
        $this->authorize('forceDelete', $task);

        $task->forceDelete();

        return redirect()->route('tasks.trashed')
            ->with('success', 'The task has been permanently deleted.');
    }
}
