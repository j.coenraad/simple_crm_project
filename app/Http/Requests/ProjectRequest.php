<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'min:5', 'max:255'],
            'description' => ['nullable', 'string', 'min:5', 'max:1000'],
            'client_id' => ['required', 'integer', Rule::exists('clients', 'id')->withoutTrashed()],
            'manager_id' => ['nullable', 'integer', Rule::exists('users', 'id')->withoutTrashed()],
            'due_date' => ['required', 'date', 'after:today', 'before:2030-12-31'],
            'status_id' => ['nullable', 'integer', 'exists:status,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'client_id.required' => 'The client field is required.',
            'client_id.integer' => 'The client must be a number.',
            'client_id.exists' => 'The selected client is invalid.',
            'manager_id.required' => 'The manager field is required.',
            'manager_id.integer' => 'The manager must be a number.',
            'manager_id.exists' => 'The selected manager is invalid.',
            'status_id.integer' =>  'The status must be a number.',
            'status_id.exists' => 'The selected status is invalid.'
        ];
    }


}
