<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => ['required', 'string', 'min:3', 'max:255'],
            'company_address' => ['required', 'string', 'min:3', 'max:255'],
            'company_zip' => ['required', 'string', 'min:5', 'max:7'],
            'company_city' => ['required', 'string', 'min:3', 'max:255'],
            'company_country' => ['required', 'string', 'min:4', 'max:255'],
            'contact_name' =>  ['required', 'string', 'min:5', 'max:255'],
            'contact_email' => ['required', 'email', 'min:5', 'max:255', Rule::unique('clients', 'contact_email')->ignore($this->contact_email)],
            'contact_phone' => ['required', 'numeric', 'digits:10'],
        ];
    }
}
