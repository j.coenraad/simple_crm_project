<?php

namespace App\Http\Requests;

use App\Rules\ActiveProjectRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:5', 'max:255'],
            'description' => ['nullable', 'string', 'min:5', 'max:1000'],
            'project_id' => ['required', 'integer', Rule::exists('projects', 'id')->withoutTrashed(), new ActiveProjectRule],
            'user_id' => ['nullable', 'integer', Rule::exists('users', 'id')->withoutTrashed()],
            'status_id' => ['nullable', 'integer', 'exists:status,id'],
        ];
    }


    /**
     * Prepare the data for validation.
     * 
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'description' => strip_tags($this->description)
        ]);
    }

    /**
     * Get the error messages for the defined validation rules
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'project_id.required' => 'The project field is required.',
            'project_id.exists' => 'The selected project is invalid.',
            'user_id.exists' => 'The selected user is invalid.',
            'status_id.exists' => 'The selected status is invalid.'
        ];
    } 
}
