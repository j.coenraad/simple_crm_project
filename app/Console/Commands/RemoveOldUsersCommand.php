<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class RemoveOldUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove the users account that are deleted more than 2 months ago';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $counter = 0;
        $users = User::onlyTrashed()->whereDate('deleted_at', '<=', now()->subMonths(2))->get();

        if(!count($users)) {
            $this->info('No discarded users found.');
            return;
        }

        foreach($users as $user) {
            $counter++;
            $user->forceDelete();
        }

        $this->info('Removed ' . $counter . ' user(s) that were discarded at least 2 months ago.');
        return;
    }
}
