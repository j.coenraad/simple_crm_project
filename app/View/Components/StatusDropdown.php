<?php

namespace App\View\Components;

use App\Models\Status;
use Illuminate\View\Component;

class StatusDropdown extends Component
{
    public $route;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.status-dropdown', [
            'states' => Status::all(),
            'currentState' => Status::firstWhere('slug', request('status'))
        ]);
    }
}
