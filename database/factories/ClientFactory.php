<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => $this->faker->company(),
            'company_address' => $this->faker->streetAddress(),
            'company_zip' => $this->faker->postcode(),
            'company_city' => $this->faker->city(),
            'company_country' => $this->faker->country(),
            'contact_name' => $this->faker->name(),
            'contact_email' => $this->faker->unique()->safeEmail(),
            'contact_phone' => $this->faker->e164PhoneNumber()
        ];
    }
}
