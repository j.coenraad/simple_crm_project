<?php

namespace Database\Factories;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'project_id' => Project::factory(),
            'user_id' => User::factory()->employee(),
            'name' => $this->faker->sentence(),
            'description' => $this->faker->sentences(4, true),
            'status_id' => 1
        ];
    }
}
