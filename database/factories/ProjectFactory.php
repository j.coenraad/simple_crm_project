<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'manager_id' => User::factory()->manager(),
            'client_id' => Client::factory(),
            'title' => $this->faker->sentence(),
            'description' => $this->faker->sentences(3, true),
            'due_date' => $this->faker->dateTimeBetween(now(), now()->addMonths(8)),
            'status_id' => 1
        ];
    }

    public function open()
    {
        return $this->state(function (array $attributes) {
            return [
                'status_id' => 1
            ];
        });
    }

    public function pending()
    {
        return $this->state(function (array $attributes) {
            return [
                'status_id' => 2
            ];
        });
    }

    public function completed()
    {
        return $this->state(function (array $attributes) {
            return [
                'status_id' => 3
            ];
        });
    }

    public function closed()
    {
        return $this->state(function (array $attributes) {
            return [
                'status_id' => 4
            ];
        });
    }


}
