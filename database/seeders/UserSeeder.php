<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->superAdmin()->create([
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('welkom123'),
            'password_changed_at' => now(),
        ]);

        User::factory()->admin()->create([
            'name' => 'Jane Doe',
            'email' => 'jane@example.com',
            'password_changed_at' => now(),
        ]);

        User::factory()->manager()->create([
            'name' => 'Mable Smith',
            'email' => 'mable.smith@example.com',
            'password_changed_at' => now(),
        ]);

        User::factory()->manager()->create([
            'name' => 'Stella Herman',
            'email' => 'stella.herman@example.com',
            'password_changed_at' => now(),
        ]);

        User::factory()->employee()->create([
            'name' => 'Amie Boyle',
            'email' => 'amie.boyle@example.com',
            'password_changed_at' => now(),
        ]);

        User::factory()->employee()->create([
            'name' => 'Elias Sterling',
            'email' => 'elias.sterling@example.com',
            'password_changed_at' => now(),
        ]);

        User::factory(4)->employee()->create();
        User::factory()->employee()->unverified()->unchangedPassword()->create();
        User::factory()->employee()->unverified()->expired_token()->create();
    }
}
