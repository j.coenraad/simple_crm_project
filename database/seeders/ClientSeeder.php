<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::factory()->create([
            'company_country' => 'The Netherlands',
            'contact_name' => 'Jake Hegmann',
            'contact_email' => 'jake.hegmann@example.com'
        ]);
        
        Client::factory()->create();

        Client::factory()->create([
            'deleted_at' => now()
        ]);
    }
}
