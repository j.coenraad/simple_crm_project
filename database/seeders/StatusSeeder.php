<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create(['name' => 'Open', 'slug' => 'open']);
        Status::create(['name' => 'Pending', 'slug' => 'pending']);
        Status::create(['name' => 'Completed', 'slug' => 'completed']);
        Status::create(['name' => 'Closed', 'slug' => 'closed']);
    }
}
