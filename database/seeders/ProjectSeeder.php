<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = Client::first();

        $sampleUser = User::role('manager')->first();

        Project::factory()->create([
            'title' => 'Crm project',
            'description' => 'A project to learn how you can create a simple crm',
            'manager_id' => $sampleUser->id,
            'client_id' => $client->id,
            'status_id' => 1
        ]);

        Project::factory()->create([
            'title' => 'Secondary project',
            'description' => 'A test project with the pending status.',
            'manager_id' => $sampleUser->id,
            'client_id' => $client->id,
            'status_id' => 2
        ]);

        Project::factory()->create([
            'title' => 'A completed project',
            'description' => 'A sample project that has been completed',
            'manager_id' => $sampleUser->id,
            'client_id' => $client->id,
            'status_id' => 3
        ]);

        Project::factory()->create([
            'title' => 'A closed project',
            'description' => 'A sample project that has been closed',
            'manager_id' => $sampleUser->id,
            'client_id' => $client->id,
            'status_id' => 4
        ]);

        Project::factory()->create([
            'title' => 'A restored project',
            'description' => 'A project that will be used to test the restore functionality',
            'manager_id' => null,
            'client_id' => $client->id,
            'status_id' => 4
        ]);

        Project::factory()->create([
            'title' => 'A project to be force deleted',
            'description' => 'This project will be used to test the force delete functionality',
            'manager_id' => null,
            'client_id' => $client->id,
            'status_id' => 4
        ]);

        $users = User::role('manager')->get();

        $users->each( function($user) use($client) {
            Project::factory()->create([
                'manager_id' => $user->id,
                'client_id' => $client->id
            ]);
        });
    }
}
