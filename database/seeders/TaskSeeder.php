<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = Project::all();

        $projects->each( function ($project){
            Task::factory(3)->for($project)->create();
        });

        $client = Client::latest()->first();

        $project = Project::factory()->create([
            'title' => 'New sample project',
            'description' => 'The tasks in this project will all have a different status.',
            'client_id' => $client->id,
            'manager_id' => User::role('manager')->first()->id,
        ]);


        Task::factory()->for($project)->create(['user_id' => 5, 'status_id'=> 1]);
        Task::factory()->for($project)->create(['user_id' => 5, 'status_id'=> 2]);
        Task::factory()->for($project)->create(['user_id' => 5, 'status_id'=> 3]);
        Task::factory()->for($project)->create(['user_id' => 5, 'status_id'=> 4]);

    }
}
