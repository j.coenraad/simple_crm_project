<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'assign_admin']);
        Permission::create(['name' => 'create_user']);
        Permission::create(['name' => 'read_user']);
        Permission::create(['name' => 'edit_user']);
        Permission::create(['name' => 'delete_user']);
        Permission::create(['name' => 'restore_user']);

        Permission::create(['name' => 'create_client']);
        Permission::create(['name' => 'read_client']);
        Permission::create(['name' => 'edit_client']);
        Permission::create(['name' => 'delete_client']);
        Permission::create(['name' => 'restore_client']);
        Permission::create(['name' => 'force_delete_client']);

        Permission::create(['name' => 'create_project']);
        Permission::create(['name' => 'read_project']);
        Permission::create(['name' => 'edit_project']);
        Permission::create(['name' => 'delete_project']);
        Permission::create(['name' => 'restore_project']);
        Permission::create(['name' => 'force_delete_project']);

        Permission::create(['name' => 'create_task']);
        Permission::create(['name' => 'read_task']);
        Permission::create(['name' => 'edit_task']);
        Permission::create(['name' => 'delete_task']);
        Permission::create(['name' => 'restore_task']);
        Permission::create(['name' => 'force_delete_task']);
        Permission::create(['name' => 'unassign_task']);

        Permission::create(['name' => 'read_trashed']);
        Permission::create(['name' => 'see_activity_log']);

        Role::create(['name' => 'super-admin']);

        Role::create(['name' => 'admin'])->givePermissionTo([
            'create_user', 'read_user', 'edit_user', 'delete_user',
            'create_client', 'read_client', 'edit_client', 'delete_client',
            'restore_client', 'force_delete_client',
            'create_project', 'read_project', 'edit_project', 'delete_project',
            'restore_project', 'force_delete_project',
            'create_task', 'read_task', 'edit_task', 'delete_task',
            'restore_task', 'force_delete_task', 'unassign_task',
            'read_trashed', 'see_activity_log'
        ]);

        Role::create(['name' => 'manager'])->givePermissionTo([
            'create_client', 'read_client', 'edit_client', 'delete_client',
            'create_project', 'read_project', 'edit_project', 'delete_project',
            'create_task', 'read_task', 'edit_task', 'delete_task',
            'restore_task', 'force_delete_task', 'unassign_task',
            'read_trashed'
        ]);

        Role::create(['name' => 'employee'])->givePermissionTo([
            'read_client', 'read_project',
            'create_task', 'read_task', 'edit_task', 'delete_task'
        ]);
    }
}
