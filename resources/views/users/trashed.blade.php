@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />
                <x-alert-danger :message="session('danger')" />

                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">
                            Trashed users
                        </h1>
                    </div>

                    <div class="card-body">
                        <div class="mb-2 ms-2">
                            @if(!$users->count())
                                <p>No trashed users yet</p>
                            @else
                                <table class="table table-responsive-sm table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>E-mail</th>
                                            <th>Deleted at</th>
                                            @if(auth()->user()->can('restore_user'))
                                                <th>Actions</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                            <tr class="align-items-center">
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->deleted_at->format('d M Y') }}</td>
                                                
                                                @can('restore_user')
                                                    <td>
                                                        <form action="{{ route('users.restore', $user) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="submit" class="btn btn-sm btn-info text-white fw-semibold" value="Restore">
                                                        </form>
                                                    </td>
                                                @endcan
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="d-flex justify-content-center">
                                    {{ $users->links() }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection