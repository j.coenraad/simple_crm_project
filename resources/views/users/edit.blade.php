@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title text-capitalize">
                        {{ $user->name }}'s account
                    </h1>
                </div>

                <div class="card-body">
                    <x-errors :errors="$errors" />

                    <form action="{{ route('users.update', $user)}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="name" class="form-label">Name</label>
                            <input 
                                type="text" 
                                id="name" 
                                name="name" 
                                class="form-control @error('name') is-invalid @enderror" 
                                value="{{ old('name', $user->name) }}"
                                required>
                        </div>

                        <div class="form-group mt-2">
                            <label for="email" class="form-label">Email</label>
                            <input 
                                type="email" 
                                id="email" 
                                name="email" 
                                class="form-control @error('email') is-invalid @enderror" 
                                value="{{ old('email', $user->email) }}"
                                required>
                        </div>

                        <div class="form-group mt-2">
                            <label for="role_id" class="form-label">Role</label>
                            <select 
                                id="role_id" 
                                name="role_id" 
                                class="form-select @error('role_id') is-invalid @enderror" 
                                @if($user->hasRole('super-admin')) disabled @endif
                                required>
                                @if($user->roles->isEmpty())
                                    <option value="" selected>Select a role...</option>
                                @endif
                                @foreach($roles as $role)     
                                    <option value="{{ $role->id }}" {{ old('role_id', $user->roles)->contains($role->id) ? 'selected' : '' }}>
                                        {{ $role->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group mt-3">
                            <div class="d-flex justify-content-end ">
                                <a href="{{ route('users.index') }}" class="btn btn-outline-info me-2">Cancel</a>
                                <input type="submit" class="btn btn-success text-white text-capitalize" value="Update account">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection