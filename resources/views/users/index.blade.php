@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')"/>

                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center mb-2">
                            <div class="col-md-4">
                                <h1 class="card-title ms-2">Users</h1>
                            </div>

                            <div class="col-md-4">
                                <x-search :action="route('users.index')" :search="$search" placeholder="Search for name or email"/>
                            </div>

                            <div class="col-md-4">
                                <div class="d-flex justify-content-end me-2">
                                    @can('create_user')
                                        <a href="{{ route('users.create') }}" class="btn btn-success text-white fw-semibold rounded me-2 text-capitalize">
                                            New user
                                        </a>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="mb-2 ms-2">
                            <table class="table table-responive-sm table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    @if (auth()->user()->can('edit_user') || auth()->user()->can('delete_user'))
                                        <th>Actions</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    @if ((auth()->user()->hasRole('super-admin') &&
                                        $user->hasRole('super-admin')) ||
                                        !$user->hasRole('super-admin'))
                                        <tr>
                                            <td>
                                                <a href="{{ route('users.show', $user) }}" class="text-reset text-decoration-none">
                                                    {{ $user->name }}
                                                </a>
                                            </td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @forelse ($user->roles as $role)
                                                    {{ $role->name }}
                                                @empty
                                                    Null
                                                @endforelse
                                            </td>

                                            @if( auth()->user()->can('edit_user') || auth()->user()->can('delete_user'))
                                                <td class="d-flex justify-content-start align-items-center">
                                                    @can('edit_user')
                                                        <a href="{{ route('users.edit', $user) }}"
                                                           class="btn btn-sm btn-info fw-semibold text-capitalize">
                                                            edit
                                                        </a>
                                                    @endcan

                                                    @if (auth()->user()->can('delete_user') && !$user->hasRole('super-admin'))
                                                        <form action="{{ route('users.destroy', $user) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this user?');">
                                                            @csrf
                                                            @method('DELETE')

                                                            <input type="submit" class="btn btn-sm btn-danger text-white fw-semibold ms-2 text-capitalize" value="delete">
                                                        </form>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                            <div class="d-flex justify-content-between align-items-center clearfix">
                                <div>
                                    {{ $users->count() .' of '. $users->total() .' users' }}
                                </div>
                                <div class="justify-content-center align-items-center">
                                    {{ $users->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
