@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <x-flash-success :message="session('success')" />
            <x-errors :errors="$errors" />

            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                        <h1 class="card-title ms-2">{{ $user->name }}</h1>
                    </div>

                    <div class="mb-2 ms-2">
                        <h2 class="card-subtitle mb-2">Details</h2>
                        <table>
                            <tr>
                                <td class="fw-bold">Email</td>
                                <td class="ps-2">{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold">Role</td>
                                <td class="ps-2">{{ $user->roles[0]['name'] }}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold">Account activated</td>
                                <td class="ps-2">{{  $user->password_changed_at ? 'Yes' : 'No' }}</td>
                            </tr>

                            @if($user->has_requested_new_token)
                                <td class="fw-bold">New token requested</td>
                                <td class="ps-2">{{ $user->new_token_requested_at->format('D d-M-Y H:i:s') }}</td>
                            @endif

                            @if($user->token_reset_at)
                                <td class="fw-bold">New token sent at</td>
                                <td class="ps-2">{{ $user->token_reset_at->format('D d-M-Y H:i') }}</td>
                            @endif
                        </table>
                    </div>

                    @if($user->has_requested_new_token)
                        <div class="mt-5">
                            <form action="{{ route('users.new-token', $user) }}" method="POST">
                                @csrf
                                <button class="btn btn-primary text-white fw-semibold btn-block">Grant new token</button>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection