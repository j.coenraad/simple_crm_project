<div class="sidebar sidebar-dark sidebar-fixed" id="sidebar">
    <div class="sidebar-brand d-none d-md-flex">
        {{ config('app.name', 'Laravel') }}
    </div>

    <ul class="sidebar-nav" data-coreui="navigation" data-simplebar>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard') }}">Dashboard</a>
        </li>

        @can('read_user')
            <li class="nav-item nav-group">
                <a class="nav-link nav-group-toggle" href="#">
                    Users
                </a>

                <ul class="nav-group-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.index') }}">
                            User overview
                        </a>
                    </li>

                    @can('create_user')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('users.create') }}">
                                Create User
                            </a>
                        </li>
                    @endcan

                    @if (auth()->user()->can('read_trashed') && auth()->user()->can('read_users'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('users.trashed') }}">
                                Discarded users
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endcan

        @can('read_client')
            <li class="nav-item nav-group">
                <a class="nav-link nav-group-toggle" href="#">
                    Clients
                </a>

                <ul class="nav-group-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('clients.index') }}">
                            Client Overview
                        </a>
                    </li>

                    @can('create_client')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('clients.create') }}">
                                Create Client
                            </a>
                        </li>
                    @endcan
                    @if (auth()->user()->can('read_trashed') && auth()->user()->can('read_client'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('clients.trashed') }}">
                                Discarded Clients
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endcan

        @can('read_project')
            <li class="nav-item nav-group">
                <a class="nav-link nav-group-toggle" href="#">
                    Projects
                </a>

                <ul class="nav-group-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('projects.index') }}">
                            Project Overview
                        </a>
                    </li>

                    @can('create_project')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('projects.create') }}">
                                Create Project
                            </a>
                        </li>
                    @endcan

                    @if (auth()->user()->can('read_trashed') && auth()->user()->can('read_project'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('projects.trashed') }}">
                                Discarded Projects
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endcan

        @can('read_task')
            <li class="nav-item nav-group">
                <a class="nav-link nav-group-toggle" href="#">
                    Tasks
                </a>

                <ul class="nav-group-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('tasks.index') }}">
                            Task Overview
                        </a>
                    </li>

                    @can('create_task')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('tasks.create') }}">
                                Create Task
                            </a>
                        </li>
                    @endcan

                    @if (auth()->user()->can('read_trashed') && auth()->user()->can('read_task'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('tasks.trashed') }}">
                                Trashed Tasks
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endcan
    </ul>
</div>
