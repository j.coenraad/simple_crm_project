<header class="header header-sticky mb-4 border-bottom border-light">
    <div class="container-fluid">
        <a class="header-brand d-md-none" href="#">
            Laravel
        </a>

        <ul class="header-nav d-none d-md-flex">
            <li class="nav-item">
                <a class="nav-link" href="#">Dashboard</a>
            </li>
        </ul>
        <ul class="header-nav ms-auto"></ul>

        <ul class="header-nav mx-2">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-coreui-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }}
                </a>
                <div class="dropdown-menu dropdown-menu-start pt-0">
                    <a class="dropdown-item" href="{{ route('profiles.show', auth()->user())}}">My profile</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); 
                            document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</header>