@component('mail::message')
# Hello {{ $name }},

The administrator has created an account for you. 
Now you will need to change the password to verify your account, you can to this by clicking the button

@component('mail::button', ['url' => $url])
Change your password
@endcomponent

Thanks,<br>
{{ config('app.name') }}

<hr>
<small>
    If you're having trouble clicking the "Change your password" button, 
    copy and paste the URL below into your web browser: {{ $url }}
</small>
@endcomponent
