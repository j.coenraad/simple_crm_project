@component('mail::message')
# Dear {{ $name }}

With this message we would like to notify you that your password has been changed.
If you did not change your password please contact the helpdesk.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
