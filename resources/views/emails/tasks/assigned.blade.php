@component('mail::message')
# A task has been assigned to you

Hello {{ $user}},

The following task has been assigned to you: {{ $title }}.

@component('mail::button', ['url' => $url])
View task here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
