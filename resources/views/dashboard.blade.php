@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card" >
                <div class="card-header">
                    <h1 class="card-title">
                        {{ __('Dashboard') }}
                    </h1>
                </div>
                <div class="card-body">
                    <h1 class="card-subtitle text-muted mb-2">Welcome {{ $user->name }}</h1>

                    <div>
                        <h4 class="card-subtitle mb-2">Your tasks ({{ count($tasks) }})</h4>
                        @if(!count($tasks))
                            <p>You have no assigned tasks yet.</p>
                        @else
                            <table class="table table-responive-sm table-striped table-border">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Project</th>
                                        <th class="text-capitalize">Last updated</th>
                                    </tr>
                                    <tbody>
                                        @foreach ($tasks as $task)
                                            <tr>
                                                <x-table-link route="tasks.show" :param="$task" :routeName="$task->name" />
                                                <td>{{ $task->project ? $task->project->title : 'Null'}}</td>
                                                <td>{{ $task->updated_at->diffForHumans() }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </thead>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
