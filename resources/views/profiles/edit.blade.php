@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="car-header">
                        <h1 class="card-title ms-3">{{ $user->name }}'s profile</h1>
                    </div>

                    <div class="card-body">
                        <x-errors :errors="$errors" />

                        <form action="{{ route('profiles.update', $user) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="form-group mt-2">
                                <label for="name" class="form-label">Name</label>
                                <input 
                                    type="text" 
                                    id="name" 
                                    name="name" 
                                    class="form-control @error('name') is-invalid @enderror" 
                                    value="{{ old('name', $user->name) }}">
                            </div>

                            <div class="form-group mt-2">
                                <label for="image" class="form-label">Select an image</label>
                                <input type="file" class="form-control @error('image') is-invalid @enderror" id="image" name="image">

                                @error('image')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group mt-2">
                                <div class="d-flex justify-content-end me-2">
                                    <a href="{{ route('profiles.show', $user) }}" class="btn btn-outline-info me-2 fw-semibold">Cancel</a>
                                    <button type="submit" class="btn btn-success text-white fw-semibold">Submit</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection