@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />

                <div class="card mh-100">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h1 class="card-title ms-2">{{ $user->name }}'s profile</h1>

                            @if (auth()->user()->id == $user->id)
                                <a href="{{ route('profiles.edit', $user) }}" class="btn btn-primary text-white fw-semibold me-2">
                                    Edit
                                </a>
                            @endif
                        </div>
                        <hr>

                        <div class="d-flex justify-content-between">
                            <div class="col-md-4">
                                <img 
                                    src="{{ $user->getFirstMedia() ? $user->getFirstMedia()->getUrl() : '/media/default/avatar.png' }}" 
                                    class="img-thumbnail mh-100 rounded" 
                                    style="min-height: 200px; max-height: 250px; max-width:200px;" 
                                    alt="#">
                            </div>

                            <div class="col-md-8">
                                <x-client-data name="Name" :data="$user->name" />
                                <x-client-data name="Email" :data="$user->email" />
                                <x-client-data name="Role" :data="$user->roles[0]['name']" />
                            </div>
                        </div>
                        <hr>

                        <div>
                            @if ($user->id == auth()->user()->id)
                                <div class="d-flex justify-content-center">
                                    <h2 class="card-title">Change your password</h2>
                                </div>

                                <div>
                                    <x-errors :errors="$errors" />


                                    <form action="{{ route('new-password.store') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label for="current_password" class="form-label">Current password</label>
                                            <input 
                                                type="password" 
                                                id="current_password" 
                                                name="current_password"
                                                class="form-control @error('current_password') is-invalid @enderror">
                                        </div>

                                        <div class="form-group mt-2">
                                            <label for="password" class="form-label">New password</label>
                                            <input 
                                                type="password" 
                                                id="password" 
                                                name="password"
                                                class="form-control @error('password') is-invalid @enderror">
                                        </div>

                                        <div class="form-group mt-2">
                                            <label for="password_confirmation" class="form-label">Confirm password</label>
                                            <input 
                                                type="password" 
                                                id="password_confirmation" 
                                                name="password_confirmation"
                                                class="form-control @error('password_confirmation') is-invalid @enderror">
                                        </div>

                                        <div class="form-group mt-2">
                                            <button type="submit" class="btn btn-success text-white fw-semibold">Change password</button>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
