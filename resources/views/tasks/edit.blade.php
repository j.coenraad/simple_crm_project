@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="mb-2">
                    <a href="{{ route('tasks.show', $task) }}" class="btn btn-ghost-info">&#xab; Return to task page</a>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">{{ $task->name }}</h1>
                    </div>

                    <div class="card-body">
                        <x-errors :errors="$errors" />

                        <form action="{{ route('tasks.update', $task) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label class="form-label" for="name">Name</label>
                                <input 
                                    type="text" 
                                    id="name" 
                                    name="name" 
                                    class="form-control @error('name') is-invalid @enderror" 
                                    value="{{ old('name', $task->name) }}"
                                    required
                                >
                            </div>

                            <div class="row mt-2">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label" for="project_id">Assigned project</label>
                                        <select
                                            id="project_id"
                                            name="project_id"
                                            class="form-select @error('project_id') is-invalid @enderror"
                                            required>
    
                                            @foreach ($projects as $project)
                                                <option value="{{ $project->id }}" {{ old('project_id', $task->project_id) == $project->id ? 'selected' : '' }}>
                                                    {{ $project->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label" for="user_id">Assigned to user</label>
                                        <select
                                            id="user_id"
                                            name="user_id"
                                            class="form-select @error('user_id') is-invalid @enderror">
                                                <option value="" selected>Select a user...</option>
                                                @foreach ($users as $user)
                                                    <option value="{{ $user->id }}" {{ old('user_id', $task->user_id) == $user->id ? 'selected' : '' }}>
                                                        {{ $user->name }}
                                                    </option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="status_id">Status</label>
                                    <select 
                                        id="status_id" 
                                        name="status_id" 
                                        class="form-select @error('status_id') is-invalid @enderror" 
                                        required>
                                        @foreach ($states as $state)
                                            <option value="{{ $state->id }}" {{ old('status_id', $task->status_id) == $state->id ? 'selected' : '' }}>
                                                {{ $state->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group mt-2">
                                <label class="form-label" for="description">Description</label>
                                <textarea
                                    id="description"
                                    name="description"
                                    class="form-control @error('description') is-invalid @enderror"
                                    cols="30"
                                    rows="8"
                                >{{ old('description', $task->description ) }}
                                </textarea>
                            </div>
                            
                            <div class="form-group mt-2">
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('tasks.show', $task) }}" class="btn btn-outline-info me-2 text-capitalize">Cancel</a>
                                    <input type="submit" class="btn btn-success text-white fw-semibold text-capitalize" value="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>



{{-- <div class="container">

            <div class="card">

                <div class="card-body">
                    <x-errors :errors="$errors"/>


                        

                        <div class="row mt-2">
                            

                            
                        </div>

                        <div class="row mt-2">
                            
                        </div>

                        

                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection