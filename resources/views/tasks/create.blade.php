@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title text-capitalize">New Task</h1>
                    </div>
                    <div class="card-body">
                        <x-errors :errors="$errors" />

                        <form action="{{ route('tasks.store') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label class="form-label" for="name">Name</label>
                                <input 
                                    type="text" 
                                    id="name" 
                                    name="name"
                                    class="form-control @error('name') is-invalid @enderror" 
                                    value="{{ old('name') }}"
                                    required>
                            </div>

                            <div class="row mt-2">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label" for="project_id">Assigned project</label>
                                        <select 
                                            id="project_id" 
                                            name="project_id"
                                            class="form-select @error('project_id') is-invalid @enderror" 
                                            required>
                                            <option value="">Select a project...</option>
                                            @foreach ($projects as $project)
                                                <option value="{{ $project->id }}" {{ old('project_id') == $project->id ? 'selected' : '' }}>
                                                    {{ $project->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label" for="user_id">Assign to user</label>
                                        <select 
                                            id="user_id" 
                                            name="user_id"
                                            class="form-select @error('user_id') is-invalid @enderror">
                                            <option value="" selected>Select a user...</option>
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}" {{ old('user_id') == $user->id ? 'selected' : '' }}>
                                                    {{ $user->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mt-2">
                                <label class="form-label" for="description">Description</label>
                                <textarea 
                                    id="description" 
                                    name="description" 
                                    class="form-control @error('description') is-invalid @enderror" 
                                    cols="30"
                                    rows="8">{{ old('description') }}</textarea>
                            </div>

                            <div class="form-group mt-3">
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('tasks.index') }}" class="btn btn-outline-info mx-2">Cancel</a>
                                    <input type="submit" class="btn btn-success fw-semibold text-white text-capitalize" value="create task">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
