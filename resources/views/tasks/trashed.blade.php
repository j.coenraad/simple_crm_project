@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />
                <x-alert-danger :message="session('danger')" />

                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h1 class="card-title">Tasks</h1>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="mb-2 ms-2">
                            <x-status-dropdown route="tasks.index" />

                            @if (!$tasks->count())
                                <p>No trashed tasks yet.</p>
                            @else
                                <table class="table table-responsive-sm table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Project</th>
                                            <th>Assigned to</th>
                                            <th>status</th>
                                            <th>Deleted at</th>
                                            @if (auth()->user()->can('restore_task') || auth()->user()->can('delete_task'))
                                                <th>Actions</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($tasks as $task)
                                            <tr class="align-items-center">
                                                <td class="fw-bolder">{{ Str::limit($task->name, 35) }}</td>
                                                <td class="fw-bolder">{{ Str::limit($task->project->title, 35) }}</td>
                                                <td>{{ $task->user ? $task->user->name : 'null' }}</td>
                                                <td>{{ $task->status->name }}</td>
                                                <td><small>{{ $task->deleted_at->format('d-m-Y H:i') }}</small></td>

                                                @if(auth()->user()->can('restore_task') || auth()->user()->can('delete_task'))
                                                    <td class="d-flex align-items-center">
                                                        @can('restore_task')
                                                        <form action="{{ route('tasks.restore', $task) }}" method="POST">
                                                                @csrf
                                                                @method('PATCH')
                                                                <input type="submit" class="btn btn-sm btn-info text-white fw-semibold text-capitalize" value="Restore">
                                                        </form>
                                                        @endcan

                                                        @can('force_delete_task')
                                                        <form action="{{ route('tasks.force-delete', $task) }}" method="POST" onsubmit="return confirm('Are you sure you want to force delete this task? This process cannot be reversed!')">
                                                                @csrf
                                                                @method('PATCH')
                                                                <input type="submit" class="btn btn-sm btn-danger text-white fw-semibold ms-2 text-capitalize" value="Force Delete">
                                                        </form>
                                                        @endcan
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="d-flex justify-content-center">
                                    {{ $tasks->links() }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
