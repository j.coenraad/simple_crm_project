@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />

                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center mb-1">
                            <div class="col-md-4">
                                <h1 class="card-title ms-2">Tasks</h1>
                            </div>

                            <div class="col-md-4">
                                <x-search :action="route('tasks.index')" :search="$search" placeholder="Search for a task by name or id"/>
                            </div>

                            <div class="col-md-4">
                                <div class="d-flex justify-content-end me-2">
                                    @can('create_task')
                                        <a href="{{ route('tasks.create') }}" class="btn btn-success text-white fw-semibold rounded me-2 text-capitalize">
                                            New Task
                                        </a>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="mb-2 ms-2">
                            <x-status-dropdown route="tasks.index" />

                            @if (!$tasks->count())
                                <p>No tasks yet.</p>
                            @else
                                <table class="table table-responsive-sm table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Project</th>
                                            <th>Assigned to</th>
                                            <th>status</th>
                                            <th>Last updated</th>
                                            @if (auth()->user()->can('edit_task') || auth()->user()->can('delete_task'))
                                                <th>Actions</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($tasks as $task)
                                            <tr class="align-items-center">
                                                <x-table-link route="tasks.show" :param="$task" :routeName="$task->name" />
                                                <x-table-link route="projects.show" :param="$task->project" :routeName="$task->project->title" />
                                                <td>{{ $task->user ? $task->user->name : 'Not assigned' }}</td>
                                                <td>{{ $task->status->name }}</td>
                                                <td><small>{{ $task->updated_at->diffForHumans() }}</small></td>

                                                <td class="d-flex align-items-center">
                                                    @can('edit_task')
                                                        <a class="btn btn-sm btn-info fw-semibold text-capitalize" href="{{ route('tasks.edit', $task) }}">
                                                            Edit
                                                        </a>
                                                    @endcan

                                                    @can('delete_task')
                                                        <form action="{{ route('tasks.destroy', $task) }}" method="POST" onsubmit="return confirm('Are you sure?');" style="display: inline-block;">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="submit" class="btn btn-sm btn-danger text-white fw-semibold ms-2 text-capitalize" value="delete">
                                                        </form>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="d-flex justify-content-between align-items-center clearfix">
                                    <div class="pl-2">
                                        <span class="fw-semibold">
                                            {{ $tasks->count() }} out of {{ $tasks->total() }} tasks
                                        </span>
                                    </div>
                                    <div class="justify-content-start">
                                        {{ $tasks->links() }}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
