@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />
                <x-errors :errors="$errors" />

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h1 class="card-title ms-2 w-75">{{ $task->name }}</h1>
                            
                            <div class="d-flex justify-content-between">
                                @can('edit_task')
                                    <a href="{{ route('tasks.edit', $task) }}" class="btn btn-primary text-white fw-semibold text-capitalize me-2">
                                        Edit Task
                                    </a>
                                @endcan

                                @if(!$task->user_id)
                                    <form action="{{ route('tasks.self-assign', $task) }}"  method="POST">
                                        @csrf
                                        @method('PATCH')
                                        <button type="submit" class="btn btn-outline-info fw-semibold me-2">
                                            Assign to me
                                        </button>
                                    </form> 
                                @endif

                                @if($task->user && auth()->user()->can('unassign_task') || $task->user_id == auth()->user()->id)
                                    <form action="{{ route('tasks.unassign', $task) }}" method="POST">
                                        @csrf
                                        @method('PATCH')
                                        <input type="submit" class="btn btn-outline-info fw-semibold me-2" value="Unassign task">
                                    </form>
                                @endif
                            </div>
                        </div>
                        <hr>

                        <div class="mb-2 ms-2">
                            <h2 class="card-subtitle mb-2">Information</h2>
                            <div>
                                <strong>Created: </strong>
                                <span>{{ $task->created_at->diffForHumans() }}</span>
                            </div>
                            @if($task->updated_at > $task->created_at )
                                <div>
                                    <strong>Last updated: </strong>
                                    <span>{{ $task->updated_at->diffForHumans() }}</span>
                                </div>
                            @endif
                            <div>
                                <strong>Assigned to: </strong>
                                <span>{{ $task->user ? $task->user->name : 'Unassigned' }}</span>
                            </div>
                            <div>
                                <strong>Project: </strong>
                                <a class="text-decoration-none fw-bolder" href="{{ route('projects.show', $task->project) }}">{{ $task->project->title }}</a>
                            </div>
                            <div>
                                <strong>Status: </strong>
                                <span>{{ $task->status->name }}</span>
                            </div>
                            <hr>

                            <div class="ms-2">
                                <h3 class="car-subtitle mb-2">Description</h3>
                                <p>{!! nl2br($task->description) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End task card --}}

                @if(count($task->activities) && auth()->user()->can('see_activity_log'))
                    <x-activity-log :activities="$task->activities" />
                @endif

            </div>
        </div>
    </div>
@endsection
