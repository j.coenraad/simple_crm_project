<div class="row mb-1">
    <label class="col-sm-3 fw-bolder">{{ $name }}</label>
    <div class="col-sm-9">
        <span>{{ $data }}</span>
    </div>
</div>