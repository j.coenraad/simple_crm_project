@if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <p class="fw-bolder">Whoops! it looks like something went wrong</p>

        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        <button type="button" class="btn-close" data-coreui-dismiss="alert"></button>
    </div>
@endif