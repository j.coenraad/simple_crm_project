<div class="container-fluid">
    <div class="row">
        <form action="{{ $action }}" method="GET" class="d-flex">
            <div class="col-9">
                <input type="text" class="form-control" name="search" placeholder="{{ $placeholder }}" value="{{ $search ?: ''}}">
            </div>
            <div class="col-3 ms-2 d-flex justify-content-between align-items-center">
                <button type="submit" class="btn btn-primary text-white fw-bold me-2">Search</button>
                <a href="{{ $action }}" class="btn btn-secondary text-white fw-bold">Reset</a>
            </div>
        </form>
    </div>
</div>
