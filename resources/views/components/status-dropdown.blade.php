<div class="mb-2">
    <div class="dropdown">
        <button class="btn btn-secondary text-white dropdown-toggle" type="button" id="dropdownMenuButton" data-coreui-toggle="dropdown" aria-expanded="false">
            {{ isset($currentState) ? $currentState->name : 'Filters' }}
        </button>
        <ul class="dropdown-menu">
            <li>
                <a class="dropdown-item" href="{{ route($route) }}">all</a>
                </li>
            @foreach ($states as $status)
                <li>
                    <a class="dropdown-item" href="{{ route('tasks.index', ['status' => $status->slug]) }}">
                        {{ $status->name }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
