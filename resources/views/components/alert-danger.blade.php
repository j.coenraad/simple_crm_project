@if(session('danger'))
    <div class="alert alert-danger  alert-dismissible fade show" role="alert">
        <strong>Warning! </strong>
        {{ $message }}
        <button type="button" class="btn-close" data-coreui-dismiss="alert" aria-label="close"></button>
    </div>
@endif