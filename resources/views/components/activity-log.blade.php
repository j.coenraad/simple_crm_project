<div class="card mt-5">
    <div class="card-body">
        <h5>Activity log</h5>
        <hr />

        <table class="table table-striped">
            <thead>
                <tr class="d-flex">
                    <th class="col-1">Event</th>
                    <th class="col-1">Key</th>
                    <th class="col-4">Old</th>
                    <th class="col-4">New</th>
                    <th class="col-2">Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($activities->sortByDesc('created_at') as $activity)
                    <tr class="d-flex">
                        <td class="col-1">{{ $activity->event }}</td>
                        <td class="col-9">
                            <div>
                                <table>
                                    @foreach ($activity->properties['attributes'] ?? [] as $attribute => $new)
                                        <tr>
                                            <td class="col-1">{{ $attribute }}</td>
                                            <td class="col-4">
                                                <span title="{{ $activity->properties['old'][$attribute] ?? ''}}">
                                                    {{ Str::limit($activity->properties['old'][$attribute] ?? '', 50) }}
                                                </span>
                                            </td>
                                            <td class="col-4">
                                                <span title="{{ $new }}">
                                                    {{ Str::limit($new, 50) }}
                                                </span>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </td>
                        <td class="col-2">
                            {{ $activity->created_at->format('d-m-Y H:i:s')}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>