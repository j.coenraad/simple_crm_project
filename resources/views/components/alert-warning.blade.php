<div class="alert alert-warning alert-dismissible fade show border" role="alert">
     <strong>Warning: </strong>{{ $message }}
     <button type="button" class="btn-close" data-coreui-dismiss="alert" aria-label="close"></button>
</div>