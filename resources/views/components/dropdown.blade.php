@props(['trigger'])

<div x-data="{show: false}" @click.away="show = false" class="relative">
    <div @click="show = ! show">
        {{ $trigger }}
    </div>

    <div x-show="show" class="px-2 rounded mt-2 overflow-auto" style="display: none;">
        {{ $slot }}
    </div>
</div>