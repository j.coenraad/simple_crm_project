@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h1 class="card-title ms-2">
                                {{ $client->company_name}}
                            </h1>

                            @can('edit_client')
                                <a href="{{ route('clients.edit', $client) }}" class="btn btn-primary text-white fw-semibold text-capitalize me-2">
                                    Edit Client
                                </a>
                            @endcan
                        </div>
                        <hr>

                        <div class="mb-2 ms-2">
                            <div class="d-flex">
                                <div class="col-md-6">
                                    <h2 class="card-subtitle">
                                        Client information
                                    </h2>

                                    <div class="mt-2">
                                        <x-client-data name="Address" :data="$client->company_address"/>
                                        <x-client-data name="Postcode" :data="$client->company_zip"/>
                                        <x-client-data name="City" :data="$client->company_city"/>
                                        <x-client-data name="Country" :data="$client->company_country"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h2 class="card-subtitle">
                                        Contact information
                                    </h2>
                                    <div class="mt-2">
                                        <x-client-data name="Name" :data="$client->contact_name"/>
                                        <x-client-data name="Email" :data="$client->contact_email"/>
                                        <x-client-data name="Phone number" :data="$client->contact_phone"/>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="mt-2">
                                <h2 class="card-title">
                                    Projects
                                </h2>
                                
                                @if(!$client->projects->count())
                                    <p>No project for this client yet.</p>
                                @else
                                    <table class="table table-responsive table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Project</th>
                                                <th>Manager</th>
                                                <th>status</th>
                                                <th>Deadline</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($projects as $project)
                                                <tr>
                                                    <x-table-link route="projects.show" :param="$project" :routeName="$project->title"/>
                                                    <td>{{ $project->manager ? $project->manager->name : 'Not assigned' }}</td>
                                                    <td>{{ $project->status->name }}</td>
                                                    <td>{{ $project->due_date->format('d M Y') }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End client card --}}

                @if(count($client->activities) && auth()->user()->can('see_activity_log'))
                    <x-activity-log :activities="$client->activities" />
                @endif
            </div>
        </div>
    </div>
@endsection
