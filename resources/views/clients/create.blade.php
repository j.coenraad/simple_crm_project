@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title text-capitalize">New Client</h1>
                    </div>

                    <div class="card-body">
                        <x-errors :errors="$errors" />

                        <form action="{{ route('clients.store') }}" method="POST">
                            @csrf
                            <h3 class="card-subtitle text-capitalize">
                                Company Information
                            </h3>

                            <div class="mt-2">
                                <div class="form-group">
                                    <label class="form-label" for="company_name">Company name</label>
                                    <input 
                                        type="text" 
                                        id="company_name" 
                                        name="company_name"
                                        class="form-control @error('company_name') is-invalid @enderror"
                                        value="{{ old('company_name') }}">
                                </div>

                                <div class="row mt-2">
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="form-label" for="company_address">Company address</label>
                                            <input 
                                                type="text" 
                                                id="company_address" 
                                                name="company_address"
                                                class="form-control @error('company_address') is-invalid @enderror"
                                                value="{{ old('company_address') }}">
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group">
                                            <label class="form-label" for="company_zip">Postcode</label>
                                            <input 
                                                type="text" 
                                                id="company_zip" 
                                                name="company_zip"
                                                class="form-control @error('company_zip') is-invalid @enderror"
                                                value="{{ old('company_zip') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="form-label" for="company_city">City</label>
                                            <input 
                                                type="text" 
                                                id="company_city" 
                                                name="company_city"
                                                class="form-control @error('company_city') is-invalid @enderror"
                                                value="{{ old('company_city') }}">
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group">
                                            <label class="form-label" for="company_country">Country</label>
                                            <input 
                                                type="text" 
                                                id="company_country" 
                                                name="company_country"
                                                class="form-control @error('company_country') is-invalid @enderror"
                                                value="{{ old('company_country') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <h3 class="card-title mt-2">
                                Contact information
                            </h3>

                            <div class="row mt-2">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label" for="contact_name">Name</label>
                                        <input 
                                            type="text" 
                                            id="contact_name" 
                                            name="contact_name"
                                            class="form-control @error('contact_name') is-invalid @enderror"
                                            value="{{ old('contact_name') }}">
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label class="form-label" for="contact_email">Email</label>
                                        <input 
                                            type="email" 
                                            id="contact_email" 
                                            name="contact_email"
                                            class="form-control @error('contact_email') is-invalid @enderror"
                                            value="{{ old('contact_email') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 mt-2">
                                <div class="form-group">
                                    <label class="form-label" for="contact_phone">Phone number</label>
                                    <input 
                                        type="text" 
                                        id="contact_phone" 
                                        name="contact_phone"
                                        class="form-control @error('contact_phone') is-invalid @enderror"
                                        value="{{ old('contact_phone') }}">
                                </div>
                            </div>

                            <div class="form-group mt-2">
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('clients.index') }}" class="btn btn-outline-info text-capitalize me-2">
                                        Cancel
                                    </a>
                                    <input type="submit" class="btn btn-success text-white fw-semibold" value="Create">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
