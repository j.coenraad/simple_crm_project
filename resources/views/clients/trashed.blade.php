@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md12">
                <x-flash-success :message="session('success')" />

                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center mb-1">
                            <h1 class="card-title ms-2">Clients</h1>
                        </div>
                    </div>

                    <div class="card-body">
                        @if(!$clients->count())
                            <p>No trashed clients yet.</p>
                        @else
                            <table class="table table-responive-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Company</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Country</th>
                                        <th>Deleted at</th>
                                        @if(auth()->user()->can('restore_client') || auth()->user()->can('force_delete_client'))
                                            <th>Actions</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($clients as $client)
                                        <tr>
                                            <x-table-link route="clients.show" :param="$client" :routeName="$client->company_name"/>
                                            <td>{{ $client->company_address}}</td>
                                            <td>{{ $client->company_city }}</td>
                                            <td>{{ $client->company_country }}</td>
                                            <td>{{ $client->deleted_at->format('d M Y')}}</td>

                                            @if(auth()->user()->can('restore_client') || auth()->user()->can('force_delete_client'))
                                                <td class="d-flex align-items-center">
                                                    @can('restore_client')
                                                        <form action="{{ route('clients.restore', $client) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="submit" class="btn btn-sm btn-info text-white fw-semibold text-capitalize" value="Restore">
                                                        </form>
                                                    @endcan

                                                    @can('force_delete_client')
                                                        <form action="{{ route('clients.force-delete', $client) }}" method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <input type="submit" class="btn btn-sm btn-danger text-white fw-semibold ms-2 text-capitalize" value="Force Delete">
                                                        </form>
                                                    @endcan
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="d-flex justify-content-center">
                                {{ $clients->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection