@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')"/>

                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center mb-1">
                            <div class="col-md-4">
                                <h1 class="card-title ms-2">Clients</h1>
                            </div>
                            <div class="col-md-4">
                                <x-search :action="route('clients.index')" :search="$search" placeholder="search by company name or contact"/>
                            </div>
                            <div class="col-md-4">
                                <div class="d-flex justify-content-end me-2">
                                    @can('create_client')
                                        <a href="{{ route('clients.create') }}" class="btn btn-success text-white fw-semibold rounded me-2 text-capitalize">
                                            New client
                                        </a>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if(!$clients->count())
                            <p>No clients yet.</p>
                        @else
                            <table class="table table-responive-sm table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Contact</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    @if(auth()->user()->can('edit_client') || auth()->user()->can('delete_client'))
                                        <th>Actions</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($clients as $client)
                                        <tr>
                                            <x-table-link route="clients.show" :param="$client" :routeName="$client->company_name"/>
                                            <td>{{ $client->contact_name }}</td>
                                            <td>{{ $client->company_address }}</td>
                                            <td>{{ $client->company_city }}</td>
                                            <td>{{ $client->company_country }}</td>

                                        @if(auth()->user()->can('edit_client') || auth()->user()->can('delete_client'))
                                            <td class="d-flex align-items-center">
                                                @can('edit_client')
                                                    <a class="btn btn-sm btn-info fw-semibold text-capitalize"
                                                       href="{{ route('clients.edit', $client) }}">
                                                        Edit
                                                    </a>
                                                @endcan

                                                @can('delete_client')
                                                    <form action="{{ route('clients.destroy', $client) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this client?');">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="submit" class="btn btn-sm btn-danger text-white fw-semibold ms-2 text-capitalize" value="Delete">
                                                    </form>
                                                @endcan
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="d-flex justify-content-between align-items-center clearfix">
                                <div class="pl-2">
                                    <span class="fw-semibold">
                                        {{ $clients->count() }} out of {{ $clients->total() }} clients
                                    </span>
                                </div>
                                <div class="justify-content-start">
                                    {{ $clients->links() }}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
