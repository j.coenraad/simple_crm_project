@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title text-capitalize">New Project</h1>
                </div>
                <div class="card-body">
                    <x-errors :errors="$errors" />

                    <form action="{{ route('projects.store') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label class="form-label" for="title">Title</label>
                            <input 
                                type="text" 
                                id="title" 
                                name="title"
                                class="form-control @error('title') is-invalid @enderror" 
                                value="{{ old('title') }}"
                                required>
                        </div>

                        <div class="form-group mt-2">
                            <label class="form-label" for="description">Description</label>
                            <textarea 
                                id="description" 
                                name="description" 
                                class="form-control @error('description') is-invalid @enderror" 
                                cols="30"
                                rows="8">{{ old('description') }}</textarea>
                        </div>

                        <div class="row mt-2">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label text-capitalize" for="user_id">Project manager</label>
                                    <select 
                                        id="manager_id" 
                                        name="manager_id"
                                        class="form-select @error('manager_id') is-invalid @enderror">
                                        <option selected value="">Select project manager...</option>
                                        @foreach ($managers as $manager)
                                            <option value="{{ $manager->id }}" {{ old('manager_id') == $manager->id ? 'selected' : '' }}>
                                                {{ $manager->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label" for="client_id">Client</label>
                                    <select 
                                        id="client_id" 
                                        name="client_id"
                                        class="form-select @error('client_id') is-invalid @enderror">
                                        <option selected value="">Select a client...</option>
                                        @foreach ($clients as $client)
                                            <option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : '' }}>
                                                {{ $client->company_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mt-2">
                            <div class="form-group">
                                <label class="form-label" for="due_date">Due date</label>
                                <input 
                                    type="date" 
                                    id="due_date" 
                                    name="due_date"
                                    class="form-control @error('due_date') is-invalid @enderror"
                                    value="{{ old('due_date') }}" 
                                    required>
                            </div>
                        </div>

                        <div class="form-group mt-3">
                            <div class="d-flex justify-content-end ">
                                <a href="{{ route('projects.index') }}" class="btn btn-outline-info me-2">Cancel</a>
                                <input type="submit" class="btn btn-success text-white text-capitalize" value="create project">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
