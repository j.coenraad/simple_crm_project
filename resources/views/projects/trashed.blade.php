@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />
                <x-alert-danger :message="session('danger')" />

                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h1 class="card-title ms-2">Projects</h1>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="mb-2 ms-2">
                            @if(!$projects->count())
                                <p>The are no trashed projects yet.</p>
                            @else
                                <table class="table table-responive-sm table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Client</th>
                                            <th>Project manager</th>
                                            <th>status</th>
                                            <th>Deleted at</th>
                                            @if (auth()->user()->can('restore_project') || auth()->user()->can('force_delete_project'))
                                                <th>Actions</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($projects as $project)
                                            <tr class="align-items-center">
                                                <td class="fw-bolder">{{ Str::limit($project->title, 35) }}</td>
                                                <td class="fw-bolder">{{ Str::limit($project->client->company_name, 35) }}</td>
                                                <td>{{ $project->manager ? $project->manager->name : 'Not assigned' }}</td>
                                                <td>{{ $project->status->name }}</td>
                                                <td><small>{{ $project->deleted_at->format('d-m-Y H:i') }}</small></td>

                                                @if(auth()->user()->can('restore_project') || auth()->user()->can('force_delete_project'))
                                                    <td class="d-flex align-items-center">
                                                        @can('restore_project')
                                                            <form action="{{ route('projects.restore', $project) }}" method="POST">
                                                                @csrf
                                                                @method('PATCH')
                                                                <input type="submit" class="btn btn-sm btn-info text-white fw-semibold text-capitalize" value="Restore">
                                                            </form>
                                                        @endcan

                                                        @can('force_delete_project')
                                                            <form action="{{ route('projects.force-delete', $project) }}" method="POST" onsubmit="return confirm('Are you sure you want to force delete this project and all related tasks? This process cannot be reversed!')">
                                                                @csrf
                                                                @method('PATCH')
                                                                <input type="submit" class="btn btn-sm btn-danger text-white fw-semibold ms-2 text-capitalize" value="Force Delete">
                                                            </form>
                                                        @endcan
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="d-flex justify-content-center">
                                    {{ $projects->links() }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
