@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />

                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center mb-1">
                            <div class="col-md-4">
                                <h1 class="card-title ms-2">Projects</h1>
                            </div>

                            <div class="col-md-4">
                                <x-search :action="route('projects.index')" :search="$search" placeholder="Search by project name" />
                            </div>

                            <div class="col-md-4">
                                <div class="d-flex justify-content-end me-2">
                                    @can('create_project')
                                        <a href="{{ route('projects.create') }}" class="btn btn-success text-white fw-semibold rounded me-2 text-capitalize">
                                            New Project
                                        </a>
                                    @endcan
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="mb-2 ms-2">
                            <x-status-dropdown route="projects.index" />

                            <table class="table table-responive-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Client</th>
                                        <th>Project manager</th>
                                        <th>Due date</th>
                                        <th>status</th>
                                        <th>Last updated</th>
                                        @if (auth()->user()->can('edit_project') ||
                                            auth()->user()->can('delete_project'))
                                            <th>Actions</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($projects as $project)
                                        <tr>
                                            <x-table-link route="projects.show" :param="$project" :routeName="$project->title" />
                                            <x-table-link route="clients.show" :param="$project->client" :routeName="$project->client->company_name" />
                                            <td>{{ $project->manager ? $project->manager->name : 'Not assigned' }}</td>
                                            <td>{{ $project->due_date ? $project->due_date->format('d M Y') : 'Null' }}</td>
                                            <td>{{ $project->status->name }}</td>
                                            <td>{{ $project->updated_at->diffForHumans() }}</td>

                                            @if(auth()->user()->can('edit_project') || auth()->user()->can('delete_project'))
                                                <td class="d-flex align-items-center">
                                                    @can('edit_project')
                                                        <a class="btn btn-sm btn-info fw-semibold text-capitalize" href="{{ route('projects.edit', $project) }}">
                                                            Edit
                                                        </a>
                                                    @endcan

                                                    @can('delete_project')
                                                        <form action="{{ route('projects.destroy', $project) }}" method="POST" onsubmit="return confirm('Are you sure?');">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="submit" class="btn btn-sm btn-danger text-white fw-semibold ms-2 text-capitalize" value="Delete">
                                                        </form>
                                                    @endcan
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="d-flex justify-content-between align-items-center">
                                <div class="pl-2">
                                    <span class="fw-semibold">
                                        {{ $projects->count() }} out of {{ $projects->total() }} projects
                                    </span>
                                </div>
                                <div>
                                    {{ $projects->links() }}
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
