@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-flash-success :message="session('success')" />

                @if (!$project->manager)
                    <x-alert-warning message="This project has not been assigned to a project manager." />
                @endif

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h1 class="card-title ms-2">{{ $project->title }}</h1>

                            @can('edit_project')
                                <a href="{{ route('projects.edit', $project) }}" class="btn btn-primary text-white fw-semibold text-capitalize me-2">
                                    Edit Project
                                </a>
                            @endcan
                        </div>
                        <hr>

                        <div class="mb-2 ms-2">
                            <h2 class="card-subtitle mb-2">Details</h2>
                            <div>
                                <span class="fw-bold">Client: </span>
                                <a href="{{ route('clients.show', $project->client) }}" class="text-decoration-none">
                                    {{ $project->client->company_name }}
                                </a>
                            </div>

                            <div>
                                <span class="fw-bold">Project manager: </span>
                                {{ $project->manager ? $project->manager->name : 'Not assigned' }}
                            </div>


                            <div>
                                <span class="fw-bolder">Due date:</span>
                                <span>{{ $project->due_date->format('d M Y') }}</span>
                            </div>
                            <hr>

                            <div>
                                <p>{{ $project->description }}</p>
                                <hr>
                            </div>

                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h3>Tasks</h3>
                            </div>

                            <div>
                                @if (!$project->tasks->count())
                                    <p>There are no tasks in this project yet.</p>
                                @else
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Assigned to</th>
                                                <th>Status</th>
                                                <th>Last updated</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($tasks as $task)
                                                <tr>
                                                    <x-table-link route="tasks.show" :param="$task" :routeName="$task->name" />
                                                    <td>{{ $task->user ? $task->user->name : 'Not assigned' }}</td>
                                                    <td>{{ $task->status->slug }}</td>
                                                    <td>{{ $task->updated_at->diffForHumans() }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    <div class="d-flex justify-content-center">
                                        {{ $tasks->links() }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End project card --}}
                
                @if(count($project->activities) && auth()->user()->can('see_activity_log'))
                    <x-activity-log :activities="$project->activities" />
                @endif
            </div>
        </div>
    </div>
@endsection
