@extends('layouts.guest')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Change password
                    </div>

                    <x-flash-success :message="session('success')" class="mt-2" />
                    
                    <div class="card-body">
                        <x-errors :errors="$errors" />

                        <form method="POST" action="{{ route('change-password.store') }}">
                            @csrf
                                 
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" autofocus>
                            </div>

                            <div class="form-group mb-3">
                                <label for="password" class="form-label">New password</label>
                                <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror">
                            </div>

                            <div class="form-group mb-3">
                                <label for="password-confirm" class="form-label">Confirm password</label>
                                <input type="password" id="password-confirm" name="password_confirmation" class="form-control">
                            </div>

                            <div class="form-group">
                                <div class="d-flex justify-content-between">
                                    <input type="submit" class="btn btn-primary btn-block fw-semibold text-white" value="Change password">
                                    @if($expires_at < now() && !$token_requested)
                                        <a href="{{ route('request-token.create', $token) }}" class="btn btn-outline-info btn-block fw-semibold">Request new token</a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection