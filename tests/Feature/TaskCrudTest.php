<?php

namespace Tests\Feature;

use App\Mail\TaskAssignedMail;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Database\Seeders\StatusSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class TaskCrudTest extends TestCase
{
    use RefreshDatabase;

    protected $employee, $admin;
    protected $openTask, $pendingTask, $completedTask, $closedTask, $deletedTask;

    public function setUp():void
    {
        parent::setUp();

        $this->setupPermissions();

        $this->employee = User::factory()->employee()->create();

        $this->admin = User::factory()->admin()->create();

        $this->seed(StatusSeeder::class);

        $this->openTask = Task::factory()->create([
            'name' => 'open task',
            'status_id' => 1
        ]);

        $this->pendingTask = Task::factory()->create([
            'name' => 'pending task',
            'status_id' => 2
        ]);
        
        $this->completedTask = Task::factory()->create([
            'name' => 'completed task',
            'status_id' => 3
        ]);

        $this->closedTask = Task::factory()->create([
            'name' => 'closed task',
            'status_id' => 4
        ]);

        $this->deletedTask = Task::factory()->create([
            'name' => 'deleted task',
            'status_id' => 4,
            'deleted_at' => now()
        ]);
    }

    public function setupPermissions()
    {
        Permission::findOrCreate('create_project');
        Permission::findOrCreate('create_task');
        Permission::findOrCreate('read_task');
        Permission::findOrCreate('edit_task');
        Permission::findOrCreate('delete_task');
        Permission::findOrCreate('restore_task');
        Permission::findOrCreate('force_delete_task');
        Permission::findOrCreate('read_trashed');

        Role::findOrCreate('admin')->givePermissionTo([
            'create_project', 'create_task', 'read_task', 'edit_task', 'delete_task',
            'restore_task', 'force_delete_task', 'read_trashed'
        ]);

        Role::findOrCreate('manager')->givePermissionTo([
            'create_project', 'create_task', 'read_task', 'edit_task', 'delete_task',
        ]);

        Role::findOrCreate('employee')->givePermissionTo([
            'create_task', 'read_task', 'edit_task', 'delete_task'  
        ]);

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function guests_cannot_visit_the_task_index_route()
    {
        $this->get(route('tasks.index'))
            ->assertRedirect(route('login'));
    }
    
    /** @test */
    public function a_user_must_change_their_password_before_the_can_visit_the_task_index_route()
    {
        $user = User::factory()->unchangedPassword()->create();
        $this->actingAs($user)->get(route('tasks.index'))
            ->assertRedirect(route('change-password.create', $user->token));
    }

    /** @test */
    public function an_authenticated_user_can_see_all_active_tasks()
    {
        $this->actingAs($this->employee)->get(route('tasks.index'))
            ->assertOk()
            ->assertSeeText([
                $this->stringLengthCap($this->openTask->name), $this->stringLengthCap($this->openTask->project->title),
                $this->stringLengthCap($this->pendingTask->name), $this->stringLengthCap($this->pendingTask->project->title),
                $this->stringLengthCap($this->completedTask->name), $this->stringLengthCap($this->completedTask->project->title),
                $this->stringLengthCap($this->closedTask->name), $this->stringLengthCap($this->closedTask->project->title),
            ])
            ->assertDontSee($this->deletedTask->name);
    }

    /** @test */
    public function an_authenticated_user_can_filter_all_tasks_to_their_status()
    {
        $this->actingAs($this->employee)->get(route('tasks.index', ['status' =>'open']))
            ->assertOk()
            ->assertSeeText([
                $this->stringLengthCap($this->openTask->name),
                $this->stringLengthCap($this->openTask->project->title),
                $this->openTask->user->name,
                $this->openTask->status->name
            ])
            ->assertDontSeeText([
                $this->stringLengthCap($this->pendingTask->name),
                $this->stringLengthCap($this->completedTask->name),
                $this->stringLengthCap($this->deletedTask->name),
            ]);
    }

    /** @test */
    public function a_user_can_search_for_a_task_by_name()
    {
        $task = Task::factory()->create(['name' => 'a sample task']);
        $anotherTask = Task::factory()->create(['name' => 'another sample task']);

        $this->actingAs($this->employee)->get(route('tasks.index', ['search' => 'sample task']))
            ->assertOk()
            ->assertSeeText([
                $this->stringLengthCap($task->name),
                $this->stringLengthCap($anotherTask->name),
            ])
            ->assertDontSeeText($this->openTask->name);
    }

    /** @test */
    public function a_user_can_read_information_of_an_active_task()
    {
        $task = Task::factory()->create();
        $this->actingAs($this->employee)->get(route('tasks.show', $task))
            ->assertOk()
            ->assertSeeText([
                $task->name, 
                $task->description, 
                $task->user->name,
                $task->project->title])
            ->assertSeeText('Edit Task');
    }

    /** @test */
    public function a_user_without_the_create_task_permission_cannot_create_a_new_task()
    {
        $user = User::factory()->create([
            'password_changed_at' => now()
        ]);

        $this->actingAs($user)->get(route('tasks.create'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $values = [
            'name' => 'A simple task',
            'description' => '',
            'project_id' => 1,
            'user_id' => $user->id,
            'status_id' => 1
        ];

        $this->actingAs($user)->post(route('tasks.store'), $values)
            ->assertForbidden();

        $this->assertDatabaseMissing('tasks', $values);
    }

    /** @test */
    public function a_user_with_the_create_task_permission_can_create_a_new_task()
    {
        $project = Project::factory()->create();
        Mail::fake();

        $values = [
            'name' => 'A simple task',
            'description' => 'some description of the project',
            'project_id' => $project->id,
            'user_id' => $this->employee->id,
            'status_id' => 1
        ];

        $this->actingAs($this->employee)->post(route('tasks.store'), $values)
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been created.');
        
        Mail::assertNothingQueued();

        $this->assertDatabaseHas('tasks', $values);
    }

    /** @test */
    public function a_user_recieves_an_email_when_a_new_task_has_been_assigned_by_a_different_user()
    {
        $this->withoutExceptionHandling();
        $project = Project::factory()->open()->create();
        $secondEmployee = User::factory()->employee()->create();
        Mail::fake();

        $values = [
            'name' => 'A simple task',
            'description' => 'some description of the project',
            'project_id' => $project->id,
            'user_id' => $this->employee->id,
            'status_id' => 1
        ];

        $this->actingAs($secondEmployee)->post(route('tasks.store'), $values)
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been created.');

        Mail::assertQueued(TaskAssignedMail::class);
        $this->assertDatabaseHas('tasks', $values);
    }

    /** @test */
    public function fields_are_required()
    {
        $values = [];

        $this->actingAs($this->employee)->post(route('tasks.store'), $values)
            ->assertSessionHasErrors([
               'name' => 'The name field is required.' ,
               'project_id' => 'The project field is required.',
            ]);
    }

    /** @test */
    public function fields_have_a_minimum_length()
    {
        $task = task::factory()->make([
            'name' => str_repeat('a', 4),
            'description' => str_repeat('b', 4)
        ])->toArray();

        $this->actingAs($this->employee)->post(route('tasks.store'), $task)
            ->assertSessionHasErrors([
                'name' => 'The name must be at least 5 characters.',
                'description' => 'The description must be at least 5 characters.'
            ]);
        
        $this->assertDatabaseMissing('tasks', $task);
    }

    /** @test */
     public function fields_have_a_maximum_length()
     {
        $task = task::factory()->make([
            'name' => str_repeat('a', 256),
            'description' => str_repeat('b', 1001)
        ])->toArray();

        $this->actingAs($this->employee)->post(route('tasks.store'), $task)
            ->assertSessionHasErrors([
                'name' => 'The name must not be greater than 255 characters.',
                'description' => 'The description must not be greater than 1000 characters.'
            ]);
        
        $this->assertDatabaseMissing('tasks', $task);
     }

    /** @test */
    public function a_task_must_be_assigned_to_a_valid_project()
    {
        $project = Project::factory()->create();
        $invalidProject = Project::factory()->create(['deleted_at' => now()]);

        $task = [
            'name' => 'Some task',
            'description' => 'here is the description',
            'project_id' => $project->id,
            'user_id' => $this->employee->id,
            'status_id' => 1
        ];

        $taskInvalidProject = [
            'name' => 'Some task',
            'description' => 'here is the description',
            'project_id' => $invalidProject->id,
            'user_id' => $this->employee->id,
            'status_id' => 1
        ];

        $this->actingAs($this->employee)->post(route('tasks.store'), $task)
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been created.');

        $this->actingAs($this->employee)->post(route('tasks.store'), $taskInvalidProject)
            ->assertSessionHasErrors([
                'project_id' => 'The selected project is invalid.'
            ]);

        $this->assertDatabaseHas('tasks', $task);
        $this->assertDatabaseMissing('tasks', $taskInvalidProject);
    }

    /** @test */
    public function a_task_can_only_be_assigned_to_an_active_project()
    {
        $openProject = Project::factory()->open()->create();
        $pendingProject = Project::factory()->pending()->create();
        $completedProject = Project::factory()->completed()->create();
        $closedProject = Project::factory()->closed()->create();

        $task = [
            'name' => 'A sample task',
            'description' =>'This task is a test',
            'status_id' => 1 //Open
        ];

        $task1 = array_merge($task, ['project_id' => $openProject->id]);
        $task2 = array_merge($task, ['project_id' => $pendingProject->id]);
        $task3 = array_merge($task, ['project_id' => $completedProject->id]);
        $task4 = array_merge($task, ['project_id' => $closedProject->id]);

        $this->actingAs($this->employee)->post(route('tasks.store'), $task1)
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been created.');

        $this->actingAs($this->employee)->post(route('tasks.store'), $task2)
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been created.');

        $this->actingAs($this->employee)->post(route('tasks.store'), $task3)
            ->assertSessionHasErrors([
                'project_id' => 'The selected project must be active.'
            ]);

        $this->actingAs($this->employee)->post(route('tasks.store'), $task4)
            ->assertSessionHasErrors([
                'project_id' => 'The selected project must be active.'
            ]);

        $this->assertDatabaseHas('tasks', $task1);
        $this->assertDatabaseHas('tasks', $task2);
        $this->assertDatabaseMissing('tasks', $task3);
        $this->assertDatabaseMissing('tasks', $task4);
        $this->assertDatabaseCount('tasks', 7);
    }

    /** @test */
    public function a_task_cannot_be_assigned_to_an_invalid_employee()
    {
        $project = Project::factory()->create();
        $deletedUser = User::factory()->create(['deleted_at' => now()]);

        $task = [
            'name' => 'some sample task',
            'description' => 'description here',
            'project_id' => $project->id,
            'status_id' => 1
        ];

        $taskWithValidUser = array_merge($task, ['user_id' => $this->employee->id]);
        $taskWithDeletedUser = array_merge($task, ['user_id' => $deletedUser->id]);
        $taskWithFakeUser = array_merge($task, ['user_id' => 99]);

        $this->actingAs($this->employee)->post(route('tasks.store'), $taskWithDeletedUser)
            ->assertSessionHasErrors(['user_id' => 'The selected user is invalid.']);

        $this->actingAs($this->employee)->post(route('tasks.store'), $taskWithFakeUser)
            ->assertSessionHasErrors(['user_id' => 'The selected user is invalid.']);

        $this->actingAs($this->employee)->post(route('tasks.store'), $taskWithValidUser)
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been created.');

        $this->actingAs($this->employee)->post(route('tasks.store'), $task)
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been created.');

        $this->assertDatabaseMissing('tasks', $taskWithDeletedUser);
        $this->assertDatabaseMissing('tasks', $taskWithFakeUser);

        $this->assertDatabaseHas('tasks', $taskWithValidUser);
        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function a_task_must_have_a_valid_status()
    {
        $project = Project::factory()->create();

        $task = [
            'name' => 'some sample task',
            'description' => 'description here',
            'project_id' => $project->id,
            'user_id' => $this->employee->id
        ];

        $taskValidStatus = array_merge($task, ['status_id' => 1]);
        $taskInvalidStatus = array_merge($task, ['status_id' => 99]);

        $this->actingAs($this->employee)->post(route('tasks.store'), $taskInvalidStatus)
          ->assertSessionHasErrors([
              'status_id' => 'The selected status is invalid.'
          ]);

        $this->actingAs($this->employee)->post(route('tasks.store'), $taskValidStatus)
          ->assertRedirect(route('tasks.index'))
          ->assertSessionHas('success', 'The task has been created.');

        $this->assertDatabaseHas('tasks', $taskValidStatus);
        $this->assertDatabaseMissing('tasks', $taskInvalidStatus);
    }

    /** @test */
    public function a_user_without_the_edit_task_permission_cannot_edit_a_task()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $task = Task::factory()->for($user)->create();

        $this->actingAs($user)->get(route('tasks.edit', $task))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $values = [
            'name' => 'some sample task',
            'description' => 'description here',
            'project_id' => $project->id,
            'user_id' => $this->employee->id,
            'status_id' => 1
        ];

        $this->actingAs($user)->put(route('tasks.update', $task), $values)
            ->assertForbidden();

        $this->assertDatabaseMissing('tasks', $values);
    }

    /** @test */
    public function a_user_with_the_edit_task_permission_can_edit_a_task()
    {
        $project = Project::factory()->create();
        $task = Task::factory()->for($this->employee)->create();

        $this->actingAs($this->employee)->get(route('tasks.edit', $task))
            ->assertOk();

        $values = [
            'name' => 'some sample task',
            'description' => 'description here',
            'project_id' => $project->id,
            'user_id' => $this->employee->id,
            'status_id' => 1
        ];

        $this->actingAs($this->employee)->put(route('tasks.update', $task), $values)
            ->assertRedirect(route('tasks.show', $task))
            ->assertSessionHas('success', 'The task has been updated.');

        $this->assertDatabaseHas('tasks', $values);
    }

    /** @test */
    public function a_user_receives_an_email_when_an_existing_task_has_been_assigned_by_a_different_user()
    {
        $secondEmployee = User::factory()->employee()->create();
        Mail::fake();

        $task = array_merge($this->openTask->toArray(), ['user_id' => $this->employee->id]);

        $this->actingAs($secondEmployee)->patch(route('tasks.update', $this->openTask), $task)
            ->assertRedirect(route('tasks.show', $this->openTask))
            ->assertSessionHas('success', 'The task has been updated.');

        Mail::assertQueued(TaskAssignedMail::class);
        $this->assertDatabaseHas('tasks', ['id' => $task['id'], 'user_id' => $task['user_id']]);
    }

    /** @test */
    public function a_user_without_the_delete_task_permission_cannot_delete_a_task()
    {
        $task = Task::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)->delete(route('tasks.destroy', $task))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->assertNotSoftDeleted($task);
    }

    /** @test */
    public function a_user_with_the_delete_task_permission_can_delete_a_task()
    {
        $task = Task::factory()->create();

        $this->actingAs($this->employee)->delete(route('tasks.destroy', $task))
            ->assertRedirect(route('tasks.index'))
            ->assertSessionHas('success', 'The task has been deleted.');

        $this->assertSoftDeleted($task);
    }

    /** @test */
    public function only_a_user_with_the_read_trashed_permission_can_reach_the_trashed_tasks_page()
    {   
        $task = Task::factory()->create(['deleted_at' => now()]);

        $this->actingAs($this->employee)->get(route('tasks.trashed'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->actingAs($this->admin)->get(route('tasks.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($task->name));   
    }

    /** @test */
    public function only_a_user_with_the_restore_task_permission_can_see_the_restore_button_on_the_trashed_tasks_page()
    {
        $task = Task::factory()->create(['deleted_at' => now()]);
        $this->employee->givePermissionTo('read_trashed');
        
        $this->actingAs($this->employee)->get(route('tasks.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($task->name))
            ->assertDontSee('Restore');

        $this->actingAs($this->admin)->get(route('tasks.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($task->name))
            ->assertSee('Restore');
    }

    /** @test */
    public function only_a_user_with_the_restore_task_permission_can_restore_a_task()
    {
        $task = Task::factory()->create(['deleted_at' => now()]);

        $this->actingAs($this->employee)->patch(route('tasks.restore', $task))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');
        
        $this->assertSoftDeleted($task);

        $this->actingAs($this->admin)->patch(route('tasks.restore', $task))
            ->assertRedirect(route('tasks.trashed'))
            ->assertSessionHas('success', 'The task has been restored!');
    }

    /** @test */
    public function a_task_cannot_be_restored_when_the_related_project_is_soft_deleted()
    {
        $project = Project::factory()->create(['deleted_at' => now()]);
        $task = Task::factory()->for($project)->create(['deleted_at' => now()]);

        $this->actingAs($this->admin)->patch(route('tasks.restore', $task))
            ->assertRedirect(route('tasks.trashed'))
            ->assertSessionHas('danger', 'Could not restore the task. The project has been deleted. To restore this task you need to restore the project first.');

        $this->assertSoftDeleted($task);
    }

    /** @test */
    public function only_a_user_with_the_force_delete_task_permission_can_see_the_force_delete_button_on_the_trashed_tasks_page()
    {
        $task = Task::factory()->create(['deleted_at' => now()]);
        $this->employee->givePermissionTo('read_trashed');

        $this->actingAs($this->employee)->get(route('tasks.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($task->name))
            ->assertDontSee('Force Delete');

        $this->actingAs($this->admin)->get(route('tasks.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($task->name))
            ->assertSee('Force Delete');
    }

    /** @test */
    public function only_a_user_with_the_force_delete_task_permission_can_permanently_delete_a_task()
    {
        $task = Task::factory()->create(['deleted_at' => now()]);

        $this->actingAs($this->employee)->patch(route('tasks.force-delete', $task))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->assertSoftDeleted($task);

        $this->actingAs($this->admin)->patch(route('tasks.force-delete', $task))
            ->assertRedirect(route('tasks.trashed'))
            ->assertSessionHas('success', 'The task has been permanently deleted.');

        $this->assertDatabaseMissing('tasks', ['id' => $task->id, 'name' => $task->name]);
    }

    protected function stringLengthCap($string)
    {
        return strlen($string) > 35 ? Str::limit($string, 35) : $string;
    }

    /** @test */
    public function a_user_can_only_see_the_assign_to_me_button_on_an_unassigned_task()
    {
        $task = Task::factory()->create(['user_id' => null]);
        $secondTask = Task::factory()->create();

        $this->actingAs($this->employee)->get(route('tasks.show', $task))
            ->assertOk()
            ->assertSee('Assign to me');

        $this->actingAs($this->employee)->get(route('tasks.show', $secondTask))
            ->assertOk()
            ->assertDontSee('Assign to me');
    }

    /** @test */
    public function a_user_can_assign_an_unassigned_task_to_themselves()
    {
        $task = Task::factory()->create(['user_id' => null]);
        
        $this->actingAs($this->employee)->patch(route('tasks.self-assign', $task))
            ->assertRedirect(route('tasks.show', $task))
            ->assertSessionHas('success', 'You have assigned the task to yourself.');
    }
}
