<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Database\Seeders\StatusSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Str;
use Tests\TestCase;

class ProjectCrudTest extends TestCase
{
    use RefreshDatabase;

    protected $manager, $employee;

    public function setUp(): void
    {
        parent::setUp();

        $this->setupPermissions();

        $this->admin = User::factory()->admin()->create([
            'password_changed_at' => now()
        ]);

        $this->manager = User::factory()->manager()->create([
            'password_changed_at' => now()
        ]);

        $this->employee = User::factory()->employee()->create([
            'password_changed_at' => now()
        ]);

        $this->seed(StatusSeeder::class);
    }

    public function setupPermissions()
    {
        Permission::findOrCreate('create_project');
        Permission::findOrCreate('read_project');
        Permission::findOrCreate('edit_project');
        Permission::findOrCreate('delete_project');
        Permission::findOrCreate('restore_project');
        Permission::findOrCreate('force_delete_project');
        Permission::findOrCreate('read_trashed');

        Role::findOrCreate('admin')->givePermissionTo([
            'create_project', 'read_project', 'edit_project', 'delete_project',
            'restore_project', 'force_delete_project', 'read_trashed'
        ]);

        Role::findOrCreate('manager')->givePermissionTo([
            'create_project', 'read_project', 'edit_project', 'delete_project'
        ]);

        Role::findOrCreate('employee')->givePermissionTo([
            'read_project'
        ]);

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function an_unauthenticated_user_cannot_reach_the_project_overview_page()
    {
        $this->get(route('projects.index'))
            ->assertRedirect(route('login'));
    }

    /** @test */
    public function a_user_must_change_their_password_before_reaching_the_project_overview_page()
    {
        $user = User::factory()->unchangedPassword()->create();

        $this->actingAs($user)->get(route('projects.index'))
            ->assertRedirect(route('change-password.create', $user->token));
    }

    /** @test */
    public function a_user_with_the_read_project_permission_can_see_all_active_projects()
    {
        $openProject = Project::factory()->open()->create();
        $pendingProject = Project::factory()->pending()->create();

        $deletedProject = Project::factory()->closed()->create([
            'deleted_at' => now(),
        ]);

        $this->assertSoftDeleted($deletedProject);

        $this->actingAs($this->employee)->get(route('projects.index'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($openProject->title))
            ->assertSeeText($this->stringLengthCap($pendingProject->title))
            ->assertDontSeeText($deletedProject->title);
    }

    /** @test */
    public function a_user_can_filter_the_project_according_to_their_status()
    {
        $openProject = Project::factory()->create(['status_id' => 1, 'manager_id' => $this->manager->id]);
        $pendingProject = Project::factory()->create(['status_id' => 2, 'manager_id' => $this->manager->id]);
        $completedProject = Project::factory()->create(['status_id' => 3, 'manager_id' => $this->manager->id]);
        $closedProject = Project::factory()->create(['status_id' => 4, 'manager_id' => $this->manager->id]);

        $this->actingAs($this->employee)->get(route('projects.index'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($openProject->title))
            ->assertSeeText($this->stringLengthCap($pendingProject->title))
            ->assertSeeText($this->stringLengthCap($completedProject->title))
            ->assertSeeText($this->stringLengthCap($closedProject->title));

        $this->actingAs($this->employee)->get(route('projects.index', ['status' => 'open']))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($openProject->title))
            ->assertDontSeeText([$pendingProject->title, $completedProject->title, $closedProject->title]);
    }

    /** @test */
    public function a_user_can_search_the_projects_by_their_title()
    {
        $project = Project::factory()->create(['title' => 'Some project made by a test']);
        $otherProject = Project::factory()->create(['title' => 'Some other project']);

        $this->actingAs($this->employee)->get(route('projects.index', ['search' => 'test']))
            ->assertOk()
            ->assertSeeText($project->title)
            ->assertDontSeeText($otherProject->title);
    }

    /** @test */
    public function only_a_user_with_the_edit_project_permission_can_see_the_edit_button_on_the_project_overview()
    {
        $project = Project::factory()->create();

        $this->actingAs($this->employee)->get(route('projects.index'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($project->title))
            ->assertDontSee('Edit');

        $this->actingAs($this->manager)->get(route('projects.index'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($project->title))
            ->assertSee('Edit');
    }

    /** @test */
    public function only_a_user_with_the_delete_project_permission_can_see_the_delete_button_on_the_project_overview_page()
    {
        $project = Project::factory()->create();

        $this->actingAs($this->employee)->get(route('projects.index'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($project->title))
            ->assertDontSee('Delete');

        $this->actingAs($this->manager)->get(route('projects.index'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($project->title))
            ->assertSee('Delete');
    }

    /** @test */
    public function a_user__with_the_read_project_permission_can_read_the_information_of_an_active_project()
    {
        $project = Project::factory()->open()->create();

        $this->actingAs($this->employee)->get(route('projects.show', $project))
            ->assertOk()
            ->assertSeeText([
                $project->title,
                $project->description,
                $project->due_date->format('d M Y'),
                $project->client->company_name
            ])
            ->assertDontSee('Edit Project');

        $this->actingAs($this->manager)->get(route('projects.show', $project))
            ->assertOk()
            ->assertSeeText([
                $project->title,
                $project->description,
                $project->due_date->format('d M Y'),
                $project->client->company_name
            ])
            ->assertSeeText('Edit Project');
    }

    /** @test */
    public function a_user_with_the_read_project_can_see_all_the_active_tasks_of_the_current_project()
    {
        $project = Project::factory()->open()->create();

        $task = Task::factory()->for($project)->create();
        $deletedTask = Task::factory()->for($project)->create([
            'deleted_at' => now()
        ]);

        $this->assertSoftDeleted($deletedTask);

        $this->actingAs($this->employee)->get(route('projects.show', $project))
            ->assertOk()
            ->assertSeeText([
                $this->stringLengthCap($task->name),
                $task->user->name,
                $task->updated_at->diffForHumans()
            ])
            ->assertDontSeeText([
                $deletedTask->name, $deletedTask->user->name,
            ]);
    }

    /** @test */
    public function a_user_without_the_create_project_permission_cannot_create_a_new_project()
    {
        $this->actingAs($this->employee)->get(route('clients.create'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $client = Client::factory()->create();

        $values = [
            'title' => 'some title',
            'description' => '',
            'user_id' => $this->employee->id,
            'due_date' => '2022-12-31',
            'status_id' => 1,
            'client_id' => $client->id
        ];

        $this->actingAs($this->employee)->post(route('projects.store'), $values)
            ->assertForbidden();

        $this->assertDatabaseMissing('projects', $values);
    }

    /** @test */
    public function a_user_with_the_create_project_permission_can_create_a_new_project()
    {
        $client = Client::factory()->create();

        $values = [
            'title' => 'some title',
            'description' => 'this project is created by ' . $this->manager->name,
            'due_date' => now()->addMonths(3),
            'manager_id' => $this->manager->id,
            'status_id' => 1,
            'client_id' => $client->id
        ];

        $this->actingAs($this->manager)->post(route('projects.store'), $values)
            ->assertRedirect(route('projects.index'))
            ->assertSessionHas('success', 'The project has been created.');

        $this->assertDatabaseHas('projects', $values);

        $this->actingAs($this->manager)->get(route('projects.index'))->assertSeeText($values['title']);
    }

    /** @test */
    public function fields_are_required()
    {
        $values = [];

        $this->actingAs($this->manager)->post(route('projects.store'), $values)
            ->assertSessionHasErrors([
                'title' => 'The title field is required.',
                'client_id' => 'The client field is required.',
                'due_date' => 'The due date field is required.',
            ]);
        
        $this->assertDatabaseCount('projects', 0);
    }

    /** @test */
    public function fields_have_a_minimum_length()
    {
        $project = Project::factory()->make([
            'title' => str_repeat('a', 4),
            'description' => str_repeat('b', 4)
        ])->toArray();

        $this->actingAs($this->manager)->post(route('projects.store'), $project)
            ->assertSessionHasErrors([
                'title' => 'The title must be at least 5 characters.',
                'description' => 'The description must be at least 5 characters.'
            ]);

        $this->assertDatabaseMissing('projects', $project);
    }

    /** @test */
    public function fields_have_a_maximum_length()
    {
        $project = Project::factory()->make([
            'title' => str_repeat('a', 256),
            'description' => str_repeat('b', 1001)
        ])->toArray();

        $this->actingAs($this->manager)->post(route('projects.store'), $project)
            ->assertSessionHasErrors([
                'title' => 'The title must not be greater than 255 characters.',
                'description' => 'The description must not be greater than 1000 characters.'
            ]);

        $this->assertDatabaseMissing('projects', $project);
    }

    /** @test */
    public function a_project_must_be_assigned_to_a_valid_client()
    {
        $inactiveClient = Client::factory()->create([
            'deleted_at' => now()
        ]);

        $project = Project::factory()->make([
            'client_id' => 99
        ])->toArray();

        $projectInactiveClient = Project::factory()->make([
            'client_id' => $inactiveClient->id
        ])->toArray();

        $this->actingAs($this->manager)->post(route('projects.store'), $project)
            ->assertSessionHasErrors([
                'client_id' => 'The selected client is invalid.'
            ]);

        $this->assertDatabaseMissing('projects', $project);

        $this->actingAs($this->manager)->post(route('projects.store'), $projectInactiveClient)
            ->assertSessionHasErrors([
                'client_id' => 'The selected client is invalid.'
            ]);

        $this->assertDatabaseMissing('projects', $projectInactiveClient);
    }


    /** @test */
    public function a_project_must_be_assigned_to_a_valid_manager()
    {
        $inactiveManager = User::factory()->manager()->create([
            'deleted_at' => now()
        ]);

        $project1 = Project::factory()->make([
            'manager_id' => 99
        ])->toArray();

        $project2 = Project::factory()->make([
            'manager_id' => $inactiveManager->id
        ])->toArray();

        $projectValidManager = Project::factory()->make([
            'manager_id' => $this->manager->id
        ])->toArray();

        $this->actingAs($this->manager)->post(route('projects.store'), $project1)
            ->assertSessionHasErrors([
                'manager_id' => 'The selected manager is invalid.'
            ]);

        $this->actingAs($this->manager)->post(route('projects.store'), $project2)
            ->assertSessionHasErrors([
                'manager_id' => 'The selected manager is invalid.'
            ]);

        $this->actingAs($this->manager)->post(route('projects.store'), $projectValidManager)
            ->assertRedirect(route('projects.index'))
            ->assertSessionHas('success', 'The project has been created.');

        $this->assertDatabaseMissing('projects', $project1);
        $this->assertDatabaseMissing('projects', $project2);
        $this->assertDatabaseHas('projects', [
            'title' => $projectValidManager['title'],
            'manager_id' => $projectValidManager['manager_id']
        ]);
    }

    /** @test */
    public function a_project_must_have_a_valid_status()
    {
        $project1 = Project::factory()->make([
            'status_id' => 99
        ])->toArray();

        $project2 = Project::factory()->make([
            'status_id' => 1
        ])->toArray();

        $this->actingAs($this->manager)->post(route('projects.store'), $project1)
            ->assertSessionHasErrors([
                'status_id' => 'The selected status is invalid.'
            ]);

        $this->actingAs($this->manager)->post(route('projects.store'), $project2)
            ->assertRedirect(route('projects.index'))
            ->assertSessionHas('success', 'The project has been created.');

        $this->assertDatabaseMissing('projects', $project1);
        $this->assertDatabaseHas('projects', [
            'title' => $project2['title'],
            'status_id' => $project2['status_id']
        ]);
    }

    /** @test */
    public function the_due_date_of_a_project_cannot_be_before_or_at_the_current_date()
    {
        $project = Project::factory()->make(['due_date' => now()->subDay(), 'manager_id' => $this->manager->id])->toArray();
        $project2 = Project::factory()->make(['due_date' => now(), 'manager_id' => $this->manager->id])->toArray();

        $this->actingAs($this->manager)->post(route('projects.store'), $project)
            ->assertSessionHasErrors([
                'due_date' => 'The due date must be a date after today.'
            ]);

        $this->assertDatabaseMissing('projects', $project);

        $this->actingAs($this->manager)->post(route('projects.store'), $project2)
            ->assertSessionHasErrors([
                'due_date' => 'The due date must be a date after today.'
            ]);

        $this->assertDatabaseMissing('projects', $project2);
    }

    /** @test */
    public function a_user_with_the_edit_project_permission_cannot_edit_a_project()
    {
        $project = Project::factory()->create();

        $this->actingAs($this->employee)->get(route('projects.edit', $project))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->actingAs($this->employee)->put(route('projects.update', $project), [
            'title' => 'The title has been changed',
            'description' => 'The description has also been changed.'
        ])->assertForbidden();

        $this->assertDatabaseMissing('projects', [
            'id' => $project->id,
            'title' => 'The title has been changed',
            'description' => 'The description has also been changed.'
        ]);
    }

    /** @test */
    public function a_user_with_the_edit_project_permission_can_edit_a_project()
    {
        $project = Project::factory()->open()->create();

        $values = [
            'title' => 'The title value has been changed',
            'description' => 'We have also changed the description',
            'status_id' => 2,
            'client_id' => $project->client_id,
            'manager_id' => $project->manager_id,
            'due_date' => $project->due_date
        ];

        $this->actingAs($this->manager)->put(route('projects.update', $project), $values)
            ->assertRedirect(route('projects.show', $project))
            ->assertSessionHas('success', 'The project has been updated.');

        $this->assertDatabaseHas('projects', $values);
    }

    /** @test */
    public function a_user_without_the_delete_project_permission_cannot_delete_a_project()
    {
        $project = Project::factory()->create();

        $this->actingAs($this->employee)->delete(route('projects.destroy', $project))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');


        $this->assertNotSoftDeleted($project);
    }

    /** @test */
    public function a_user_with_the_delete_project_permission_can_delete_a_project()
    {
        $project = Project::factory()->create();

        $this->actingAs($this->manager)->delete(route('projects.destroy', $project))
            ->assertRedirect(route('projects.index'))
            ->assertSessionHas('success', 'The project has been deleted.');

        $this->assertSoftDeleted($project);

        $this->get(route('projects.index'))->assertDontSeeText($project->title);
    }

    /** @test */
    public function only_a_user_with_the_read_trashed_permission_can_see_projects_that_have_been_trashed()
    {
        $this->actingAs($this->admin)->get(route('projects.trashed'))
            ->assertOk()
            ->assertSeeText('The are no trashed projects yet.');

        $project = Project::factory()->create(['manager_id' => $this->manager->id]);
        $deletedProject = Project::factory()->create(['manager_id' => $this->manager->id, 'deleted_at' => now()]);

        $this->assertNotSoftDeleted($project);
        $this->assertSoftDeleted($deletedProject);

        $this->actingAs($this->admin)->get(route('projects.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($deletedProject->title))
            ->assertDontSee($project->title);
    }

    /** @test */
    public function only_a_user_with_the_restore_project_permission_can_see_the_restore_button_on_the_trashed_project_overview()
    {
        $user = User::factory()->create();
        $project = Project::factory()->create(['manager_id' => $this->manager->id]);
        $deletedProject = Project::factory()->create(['manager_id' => $this->manager->id, 'deleted_at' => now()]);

        $this->actingAs($user)->get(route('projects.trashed'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->actingAs($this->admin)->get(route('projects.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($deletedProject->title))
            ->assertDontSee($project->title);
    }

    /** @test */
    public function only_a_user_with_the_restore_project_permission_can_restore_a_project()
    {
        $project = Project::factory()->create(['deleted_at' => now()]);

        $this->actingAs($this->manager)->patch(route('projects.restore', $project))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->assertSoftDeleted($project);

        $this->actingAs($this->admin)->patch(route('projects.restore', $project))
            ->assertRedirect(route('projects.trashed'))
            ->assertSessionHas('success', 'The project has been restored!');

        $this->assertNotSoftDeleted($project);

        $this->actingAs($this->admin)->get(route('projects.index'))
            ->assertSeeText($this->stringLengthCap($project->title));
    }

    /** @test */
    public function a_project_cannot_be_restored_when_the_client_is_soft_deleted()
    {
        $client = Client::factory()->create(['deleted_at' => now()]);
        $project = Project::factory()->for($client)->create(['deleted_at' => now()]);

        $this->actingAs($this->admin)->patch(route('projects.restore', $project))
            ->assertRedirect(route('projects.trashed'))
            ->assertSessionHas('danger', 'Could not restore the project. The client has been deleted. You need to restore the client before you can restore the project!');

        $this->assertSoftDeleted($project);
    }

    /** @test */
    public function only_a_user_with_the_force_delete_project_permission_can_see_the_force_delete_button_on_the_trashed_projects_overview()
    {
        $manager = $this->manager->givePermissionTo('read_trashed');
        $project = Project::factory()->create(['deleted_at' => now()]);

        $this->actingAs($manager)->get(route('projects.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($project->title))
            ->assertDontSee('Force Delete');

        $this->actingAs($this->admin)->get(route('projects.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($project->title))
            ->assertSee('Force Delete');
    }

    /** @test */
    public function only_users_with_the_force_delete_project_permission_can_permanently_delete_a_project()
    {
        $project = Project::factory()->create(['deleted_at' => now()]);

        $this->actingAs($this->manager)->patch(route('projects.force-delete', $project))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->assertSoftDeleted($project);

        $this->actingAs($this->admin)->patch(route('projects.force-delete', $project))
            ->assertRedirect(route('projects.trashed'))
            ->assertSessionHas('success', 'The project has been permanently deleted.');

        $this->assertDatabaseMissing('projects', ['id' => $project->id, 'title' => $project->title]);
    }

    /** @test */
    public function when_a_project_is_permanently_deleted_the_related_task_are_also_deleted()
    {
        $project = Project::factory()->create(['deleted_at' => now()]);
        $task = Task::factory()->for($project)->create(['deleted_at' => now()]);

        $this->actingAs($this->admin)->patch(route('projects.force-delete', $project))
            ->assertRedirect(route('projects.trashed'))
            ->assertSessionHas('success', 'The project has been permanently deleted.');

        $this->assertDatabaseMissing('projects', ['id' => $project->id, 'title' => $project->title]);
        $this->assertDatabaseMissing('tasks', ['id' => $task->id, 'name' => $task->name]);
    }

    protected function stringLengthCap($string)
    {
        return strlen($string) > 35 ? Str::limit($string, 35) : $string;
    }
}
