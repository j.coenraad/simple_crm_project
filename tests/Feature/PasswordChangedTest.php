<?php

namespace Tests\Feature;

use App\Models\User;
use App\Notifications\NewTokenRequested;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class PasswordChangedTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $admin;

    public function setUp():void
    {
        parent::setUp();

        $this->setupPermissions();

        $this->user = User::factory()->create();
        $this->admin = User::factory()->admin()->create();

        $this->unchanged_user = User::factory()->unchangedPassword()->unverified()->create();
    }

    private function setupPermissions()
    {
        Permission::findOrCreate('read_user');
        Permission::findOrCreate('edit_user');

        Role::findOrCreate('employee');
        Role::findOrCreate('admin')->givePermissionTo([
            'read_user', 'edit_user'
        ]);
    }

    /** @test */
    public function a_user_cannot_reach_the_dashboard_without_changing_their_password()
    {
        $this->actingAs($this->unchanged_user)->get(route('dashboard'))
            ->assertRedirect(route('change-password.create', $this->unchanged_user->token));
    }

    /** @test */
    public function a_visitor_can_only_reach_the_change_password_page_with_a_token()
    {
        $this->get(route('change-password.create', $this->unchanged_user->token))
            ->assertStatus(200);
    }

    /** @test */
    public function a_user_that_has_their_password_changed_cannot_reach_the_change_password_page()
    {
        $this->get(route('change-password.create', $this->user->token))
            ->assertRedirect(route('login'));
    }

    /** @test */
    public function fields_are_required()
    {
        $values = [
            'token' => $this->user->token
        ];

        $this->post(route('change-password.store'), $values)
            ->assertSessionHasErrors([
                'email' => 'The email field is required.',
                'password' => 'The password field is required.'
            ]);
    }

    /** @test */
    public function the_email_must_be_a_valid_email_address()
    {
        $user_fake = User::factory()->unchangedPassword()->create();

        $this->post(route('change-password.store'), [
            'token' => $user_fake->token,
            'email' => 'user.fake_example.com',
            'password' => 'wilkommen123',
            'password_confirmation' => 'wilkommen123'
        ])->assertSessionHasErrors([
            'email' => 'The email must be a valid email address.'
        ]);
    }

    /** @test */
    public function the_email_must_exist_in_the_database()
    {
        $user = User::factory()->unchangedPassword()->create(['email' => 'sample.user@example.com']);

        $this->post(route('change-password.store'), [
            'token' => $user->token,
            'email' => 'simple.user@example.com',
            'password' => 'wilkommen123',
            'password_confirmation' => 'wilkommen123'
        ])->assertSessionHasErrors([
            'email' => 'The selected email is invalid.'
        ]);


        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'email' => 'simple.user@example.com',
            'password_changed_at' => now()
        ]);
    }

    /** @test */
    public function the_email_and_token_must_belong_to_the_same_user()
    {
        $user1 = User::factory()->unchangedPassword()->create(['email' => 'user1@example.com', 'token' => '9N3G9IVklVPWQovt8pYz04ld7eUO6ljaoKabYiZZ9hsPfJGLjvYIDJA98DFx']);
        $user2 = User::factory()->unchangedPassword()->create(['email' => 'user2@example.com', 'token' => 'Y30umX4mwleUAF6YaVIgGPntwnZfUzy3QTIY8qfIceN3SasNRqtsDlh5YTE5']);

        $this->post(route('change-password.store'), [
            'token' => $user1->token,
            'email' => $user2->email,
            'password' => 'wilkommen123',
            'password_confirmation' => 'wilkommen123'
        ])->assertSessionHasErrors([
            'email' => 'The selected email is invalid.',
        ]);

        $this->assertDatabaseMissing('users', [
            'email' => $user1->email,
            'token' => $user1->token,
            'password_changed_at' => now()
        ]);

        $this->assertDatabaseMissing('users', [
            'email' => $user2->email,
            'token' => $user2->token,
            'password_changed_at' => now()
        ]);
    }

    /** @test */
    public function password_and_password_confirm_must_match()
    {
        $user = User::factory()->unchangedPassword()->create();

        $this->post(route('change-password.store'), [
            'token' => $user->token,
            'email' => $user->email,
            'password' => 'Pa$$w0rd',
            'password_confirmation' => 'Password'
        ])->assertSessionHasErrors([
            'password' => 'The password confirmation does not match.'
        ]);

        $this->assertDatabaseMissing('users', [
            'email' => $user->email,
            'token' => $user->token,
            'password_changed_at' => now()
        ]);
    }

    /** @test */
    public function the_user_token_may_not_be_expired()
    {
        $user = User::factory()->expiredToken()->create();
        $tmp = $user->updated_at;

        $this->post(route('change-password.store'), [
            'email' => $user->email,
            'password' => 'Pa$$word',
            'password_confirmation' => 'Pa$$word',
            'token' => $user->token,
        ])
            ->assertRedirect(route('change-password.create', $user->token))
            ->assertSessionHasErrors([
                'error' => 'The token has expired. Please request a new token to activate your account.'
            ]);

        $this->assertDatabaseHas('users', ['id' => $user->id, 'updated_at' => $tmp]);
    }

    /** @test */
    public function a_visitor_can_see_the_request_new_token_button_when_the_user_token_has_expired()
    {
        $user = User::factory()->unchangedPassword()->expiredToken()->create();
        $secondUser = User::factory()->unchangedPassword()->create();

        $this->get(route('change-password.create', $user->token))
            ->assertOK()
            ->assertSee('Request new token');

        $this->get(route('change-password.create', $secondUser->token))
            ->assertOk()
            ->assertDontSee('Request new token');
    }

    /** @test */
    public function a_user_with_an_activated_account_cannot_reach_the_request_token_page()
    {
        $user = User::factory()->employee()->create();

        $this->actingAs($user)->get(route('request-token.create', $user->token))
            ->assertRedirect(route('dashboard'));
    }

    /** @test */
    public function a_visitor_can_only_reach_the_request_token_page_when_the_user_token_has_expired()
    {
        $user = User::factory()->expiredToken()->create();
        $secondUser = User::factory()->unchangedPassword()->create();

        $this->get(route('request-token.create', $user->token))
            ->assertOk();

        $this->get(route('request-token.create', $secondUser->token))
            ->assertRedirect(route('change-password.create', $secondUser->token))
            ->assertSessionHasErrors(['error' => 'The token has not yet expired.']);
    }

    /** @test */
    public function a_visitor_can_not_reach_the_request_token_page_when_a_new_token_has_already_been_requested()
    {
        $user = User::factory()->expiredToken()->create(['new_token_requested_at' => now()->subMinute()]);

        $this->get(route('request-token.create', $user->token))
            ->assertRedirect(route('change-password.create', $user->token))
            ->assertSessionHasErrors(['error' => 'A new token has already been requested.']);
    }

    /** @test */
    public function the_visitor_must_enter_a_valid_email_to_request_a_new_token()
    {
        $user = User::factory()->unchangedPassword()->expiredToken()->create();

        $values = [
            'token' => $user->token,
            'email' => 'sample.test'
        ];

        $this->post(route('request-token.store'), $values)
            ->assertSessionHasErrors([
                'email' => 'The email must be a valid email address.'
            ]);

        $this->assertDatabaseHas('users', ['id' => $user->id, 'new_token_requested_at' => null]);
    }

    /** @test */
    public function the_visitor_must_enter_an_existing_email_address_to_request_a_new_token()
    {
        $user = User::factory()->unchangedPassword()->expiredToken()->create([
            'email' => 'john.doe@example.com'
        ]);

        $values = [
            'token' => $user->token,
            'email' => 'jane.doe@example.com'
        ];

        $this->post(route('request-token.store'), $values)
            ->assertSessionHasErrors([
                'email' => 'The selected email is invalid.'
            ]);

        
        $this->assertDatabaseHas('users', ['id' => $user->id, 'new_token_requested_at' => null]);
    }

    /** @test */
    public function the_token_and_email_must_match_to_request_a_new_token()
    {
        $user = User::factory()->expiredToken()->create();
        $anotherUser = User::factory()->expiredToken()->create();

        $values = [
            'email' => $user->email,
            'token' => $user->token,
        ];

        $wrongValues = [
            'email' => $user->email,
            'token' => $anotherUser->token
        ];

        $this->post(route('request-token.store'), $wrongValues)
            ->assertSessionHasErrors([
                'email' => 'The selected email is invalid.'
            ]);

        $this->post(route('request-token.store', $values))
            ->assertRedirect(route('change-password.create', $user->fresh()->token))
            ->assertSessionHas('success', 'A new token has been requested.');

        $user->refresh();
        $this->assertNotNull($user->new_token_requested_at);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'email' => $user->email,
        ]);
    }

    /** @test */
    public function a_visitor_with_an_expired_user_token_can_only_request_a_new_token_once()
    {
        $user = User::factory()->expiredToken()->create(['new_token_requested_at' => now()->subMinute()]);
        $token_requested_at = $user->new_token_requested_at;
        
        $values = [
            'email' => $user->email,
            'token' => $user->token,
        ];

        $this->post(route('request-token.store'), $values)
            ->assertRedirect(route('change-password.create', $user->token))
            ->assertSessionHasErrors(['error' => 'A new token has already been requested.']);

        $user->refresh();

        $this->assertEquals($user->new_token_requested_at, $token_requested_at);
    }

    /** @test */
    public function when_a_visitor_requests_a_new_token_a_notification_is_sent_to_the_admin()
    {
        $user = User::factory()->expiredToken()->create();
        Notification::fake();

        $values = [
            'email' => $user->email,
            'token' => $user->token,
        ];

        $this->post(route('request-token.store'), $values)
            ->assertRedirect(route('change-password.create', $user->token))
            ->assertSessionHas('success', 'A new token has been requested.');

        Notification::assertSentTo($this->admin, NewTokenRequested::class);

        $user->refresh();
        $this->assertNotNull($user->new_token_requested_at);
    }

    /** @test */
    public function an_admin_can_see_the_grant_token_button_when_a_user_requests_a_new_token()
    {
        $user = User::factory()->employee()->expiredToken()->create(['new_token_requested_at' => now()->subMinute()]);
        $anotherUser = User::factory()->employee()->expiredToken()->create();

        $this->actingAs($this->admin)->get(route('users.show', $user))
            ->assertOk()
            ->assertSee('Grant new token');

        $this->actingAs($this->admin)->get(route('users.show', $anotherUser))
            ->assertOk()
            ->assertDontSee('Grant new token');
    }

    /** @test */
    public function an_admin_can_only_grant_a_user_a_new_token_when_a_token_was_requested()
    {
        $user = User::factory()->employee()->expiredToken()->create(['new_token_requested_at' => now()->subMinute()]);
        $oldToken = $user->token;
        $anotherUser = User::factory()->employee()->expiredToken()->create();
        $secondOldToken = $anotherUser->token;

        $this->actingAs($this->admin)->post(route('users.new-token', $user))
            ->assertRedirect(route('users.show', $user))
            ->assertSessionHas('success', 'A new token has been created.');

        $user->refresh();
        $this->assertNotEquals($user->token, $oldToken);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'new_token_requested_at' => null,
            'token_reset_at' => now(),
            'token_expires_at' => now()->addDays(3)
        ]);

        $this->actingAs($this->admin)->post(route('users.new-token', $anotherUser))
            ->assertRedirect(route('users.show', $anotherUser))
            ->assertSessionHasErrors(['error' => 'This user has not requested a new token.']);

        $anotherUser->refresh();
        $this->assertNull($anotherUser->new_token_reset_at);
    }

    /** @test */
    public function a_user_can_change_their_own_password()
    {
        $user = User::factory()->unchangedPassword()->create(['password' => bcrypt('welkom123')]);

        $this->post(route('change-password.store'), [
            'token' => $user->token,
            'email' => $user->email,
            'password' => 'Pa$$word123',
            'password_confirmation' => 'Pa$$word123'
        ])->assertRedirect(route('login'));

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'token' => null,
            'email' => $user->email,
        ]);

        $this->assertTrue(Hash::check('Pa$$word123', $user->fresh()->password));
    }
}
