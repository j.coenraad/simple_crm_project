<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Project;
use App\Models\User;
use Database\Seeders\StatusSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Str;
use Tests\TestCase;

class ClientCrudTest extends TestCase
{
    use RefreshDatabase;

    protected $manager, $employee;

    public function setUp():void
    {
        parent::setUp();
        $this->setupPermissions();

        $this->admin = User::factory()->admin()->create();

        $this->manager = User::factory()->manager()->create([
            'password_changed_at' => now()
        ]);

        $this->employee = User::factory()->employee()->create([
            'password_changed_at' => now()
        ]);

        $this->seed(StatusSeeder::class);
    }

    public function setupPermissions()
    {
        Permission::findOrCreate('create_client');
        Permission::findOrCreate('read_client');
        Permission::findOrCreate('edit_client');
        Permission::findOrCreate('delete_client');
        Permission::findOrCreate('restore_client');
        Permission::findOrCreate('force_delete_client');
        Permission::findOrCreate('read_trashed');

        Role::findOrCreate('admin')->givePermissionTo([
            'create_client', 'read_client', 'edit_client', 'delete_client',
            'restore_client', 'force_delete_client', 'read_trashed'
        ]);

        Role::findOrCreate('manager')->givePermissionTo([
            'create_client', 'read_client', 'edit_client', 'delete_client'
        ]);

        Role::findOrCreate('employee')->givePermissionTo([
            'read_client'
        ]);

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function guests_cannot_visit_the_clients_index_route()
    {
        $this->get('clients')->assertRedirect('login');
    }

    /** @test */
    public function a_user_with_an_unchanged_password_must_change_its_password()
    {
        $user = User::factory()->unchangedPassword()->create();
        $this->actingAs($user)->get(route('clients.index'))
            ->assertRedirect(route('change-password.create', $user->token));
    }

    /** @test */
    public function user_with_the_read_client_permission_can_see_all_active_clients()
    {
        $firstClient = Client::factory()->create();
        $secondClient = Client::factory()->create();
        $deletedClient = Client::factory()->create(['deleted_at' => now()]);

        $this->assertSoftDeleted($deletedClient);

        $this->actingAs($this->employee)->get(route('clients.index'))
            ->assertStatus(200)
            ->assertSeeText([$firstClient->company_name, $firstClient->company_address])
            ->assertSeeText([$secondClient->company_name, $firstClient->company_address])
            ->assertDontSeeText($deletedClient->company_name);
    }

    /** @test */
    public function a_user_can_search_for_a_client_by_company_name_or_contact()
    {
        $client1 = Client::factory()->create(['company_name' => 'First client', 'contact_name' => 'John Doe']);
        $client2 = Client::factory()->create(['company_name' => 'Second client', 'contact_name' => 'Jane Doe']);
        $client3 = Client::factory()->create(['company_name' => 'Some company', 'contact_name' => 'Taylor otwell']);

        $this->actingAs($this->employee)->get(route('clients.index', ['search' => 'client']))
            ->assertOk()
            ->assertSeeText([
                $client1->company_name, $client1->contact_name,
                $client2->company_name, $client2->contact_name,
            ])->assertDontSeeText([
                $client3->company_name, $client3->contact_name
            ]);


        $this->actingAs($this->employee)->get(route('clients.index', ['search' => 'taylor']))
            ->assertOk()
            ->assertSeeText([
                $client3->company_name, $client3->contact_name
            ])->assertDontSeeText([
                $client1->company_name, $client1->contact_name,
                $client2->company_name, $client2->contact_name,
            ]);
    }

    /** @test */
    public function only_a_user_with_the_right_permission_can_see_the_actions_on_the_client_overview_page()
    {
        $client1 = Client::factory()->create();

        $this->actingAs($this->employee)->get(route('clients.index'))
            ->assertStatus(200)
            ->assertSeeText($client1->company_name)
            ->assertDontSee(['edit', 'delete']);

        $this->actingAs($this->manager)->get(route('clients.index'))
            ->assertStatus(200)
            ->assertSeeText($client1->company_name)
            ->assertSee(['edit', 'delete'])
            ;
    }

    /** @test */
    public function a_user_can_read_the_information_of_an_active_client()
    {
        $client = Client::factory()->create();
        $deletedClient = Client::factory()->create([
            'deleted_at' => now()
        ]);

        $this->assertSoftDeleted($deletedClient);

        $this->actingAs($this->manager)->get(route('clients.show', $client))
            ->assertStatus(200)
            ->assertSeeText([
                $client->company_name, $client->company_address,
                $client->contact_name, $client->contact_email, $client->contact_phone
            ]);

        $this->actingAs($this->manager)->get(route('clients.show', $deletedClient))
            ->assertStatus(404)
            ->assertSee('This page has not been found.');
    }

     /** @test */
     public function a_user_can_see_all_active_projects_of_the_current_client()
     {
        $client = Client::factory()->create();
        $firstProject = Project::factory()->for($client)->create(['status_id' => 1]);
        $deletedProject = Project::factory()->for($client)->create(['deleted_at' => now(), 'status_id' => 1]);

        $this->assertSoftDeleted($deletedProject);

        $this->actingAs($this->manager)->get(route('clients.show', $client))
            ->assertStatus(200)
            ->assertSee(strlen($firstProject->title) > 35 ? Str::limit($firstProject->title, 35) : $firstProject->title)
            ->assertDontSee($deletedProject->title);

     }

    /** @test */
    public function a_user_without_the_create_client_permission_cannot_create_a_new_client()
    {
        $this->actingAs($this->employee)->get(route('clients.create'))
            ->assertStatus(403)
            ->assertSee('User does not have the right permissions.');

        $client = Client::factory()->make()->toArray();
        $this->actingAs($this->employee)->post(route('clients.store'), $client)
            ->assertStatus(403);

        $this->assertDatabaseMissing('clients', $client);
    }

    /** @test */
    public function a_user_with_the_create_client_permission_can_create_a_new_client()
    {
        $client = [
            'company_name' => 'Sample Client B.V.',
            'company_address' => 'somewhere street 14',
            'company_zip' => '12345',
            'company_city' => 'Antwerpen',
            'company_country' => 'Belgium',
            'contact_name' => 'Bob User',
            'contact_email' => 'bob.user@example.com',
            'contact_phone' => '1234567890',
        ];

        $this->actingAs($this->manager)->post(route('clients.store'), $client)
            ->assertRedirect(route('clients.index'))
            ->assertSessionHas('success', 'The client has been created.');
        
        $this->assertDatabaseHas('clients', $client);

        $this->get(route('clients.index'))->assertSee($client['company_name']);

    }

    /** @test */
    public function fields_are_required()
    {
        $values = [];

        $this->actingAs($this->manager)->post(route('clients.store'), $values)
            ->assertSessionHasErrors([
                'company_name' => 'The company name field is required.',
                'company_address' => 'The company address field is required.',
                'company_zip' => 'The company zip field is required.',
                'company_city' => 'The company city field is required.',
                'company_country' => 'The company country field is required.',
                'contact_name' => 'The contact name field is required.',
                'contact_email' => 'The contact email field is required.',
                'contact_phone' => 'The contact phone field is required.'
            ]);
        
            $this->assertDatabaseMissing('clients', $values);
    }

    /** @test */
    public function fields_have_a_minimum_length()
    {
        $values = Client::make([
            'company_name' => str_repeat('a', 2),
            'company_address' => str_repeat('b', 2),
            'company_zip' => str_repeat('c', 4),
            'company_city' => str_repeat('d', 2),
            'company_country' => str_repeat('e',3),
            'contact_name' => str_repeat('f', 4),
            'contact_email' => str_repeat('g', 4),
            'contact_phone' => str_repeat('h', 7),
        ])->toArray();

        $this->actingAs($this->manager)->post(route('clients.store'), $values)
            ->assertSessionHasErrors([
                'company_name' => 'The company name must be at least 3 characters.',
                'company_address' => 'The company address must be at least 3 characters.',
                'company_zip' => 'The company zip must be at least 5 characters.',
                'company_city' => 'The company city must be at least 3 characters.',
                'company_country' => 'The company country must be at least 4 characters.',
                'contact_name' => 'The contact name must be at least 5 characters.',
                'contact_email' => 'The contact email must be at least 5 characters.',
                'contact_phone' => 'The contact phone must be 10 digits.',
                'contact_phone' => 'The contact phone must be a number.'
            ]);

        $this->assertDatabaseMissing('clients', $values);
    }

    /** @test */
    public function fields_have_a_maximum_length()
    {
        $values = Client::make([
            'company_name' => str_repeat('a', 256),
            'company_address' => str_repeat('b', 256),
            'company_zip' => str_repeat('c', 12),
            'company_city' => str_repeat('d', 256),
            'company_country' => str_repeat('e',256),
            'contact_name' => str_repeat('f', 256),
            'contact_email' => str_repeat('g', 256),
            'contact_phone' => str_repeat('1', 12),
        ])->toArray();

        $this->actingAs($this->manager)->post(route('clients.store'), $values)
            ->assertSessionHasErrors([
                'company_name' => 'The company name must not be greater than 255 characters.',
                'company_address' => 'The company address must not be greater than 255 characters.',
                'company_zip' => 'The company zip must not be greater than 7 characters.',
                'company_city' => 'The company city must not be greater than 255 characters.',
                'company_country' => 'The company country must not be greater than 255 characters.',
                'contact_name' => 'The contact name must not be greater than 255 characters.',
                'contact_email' => 'The contact email must not be greater than 255 characters.',
                'contact_phone' => 'The contact phone must be 10 digits.',
            ]);
        
        $this->assertDatabaseMissing('clients', $values);
    }

    /** @test */
    public function a_client_must_have_a_valid_contact_email()
    {
        $client = Client::factory()->make(['contact_email' => 'invalid.email'])->toArray();
        
        $this->actingAs($this->manager)->post(route('clients.store'), $client)
            ->assertSessionHasErrors([
                'contact_email' => 'The contact email must be a valid email address.'
            ]);
        
        $this->assertDatabaseMissing('clients', $client);
    }

    /** @test */
    public function a_client_must_have_a_unique_email()
    {
        $client = Client::factory()->create(['contact_email' => 'sample.client@example.com']);
        $secondClient = Client::factory()->make(['contact_email' => 'sample.client@example.com'])->toArray();

        $this->actingAs($this->manager)->post(route('clients.store'), $secondClient)
            ->assertSessionHasErrors([
                'contact_email' => 'The contact email has already been taken.'
            ]);
        
        $this->assertDatabaseMissing('clients', $secondClient);
    }

    /** @test */
    public function a_user_without_the_edit_client_permission_cannot_edit_a_client()
    {
        $client = Client::factory()->create();

        $this->actingAs($this->employee)->get(route('clients.edit', $client))
            ->assertStatus(403)
            ->assertSee('User does not have the right permissions.');
        
        $this->actingAs($this->employee)->put(route('clients.update', $client), [
            'company_name' => 'Changed company name',
            'company_address' => 'Some street 15',
            'company_zip' => '42657',
            'company_city' => 'Rotterdam'
        ])->assertStatus(403);

        $this->assertDatabaseMissing('clients', [
            'company_name' => 'Changed company name',
            'company_address' => 'Some street 15',
            'company_zip' => '42657',
            'company_city' => 'Rotterdam'
        ]);
    }

    /** @test */
    public function a_user_with_the_edit_client_permission_can_edit_a_client()
    {
        $client = Client::factory()->create();

        $values = [
            'company_name' => 'Changed company name',
            'company_address' => 'Some street 15',
            'company_zip' => '42657',
            'company_city' => 'Rotterdam',
            'company_country' => 'The Netherlands',
            'contact_name' => 'Bob de Bouwer',
            'contact_email' => 'bob.debouwer@example.com',
            'contact_phone' => '0123456789'
        ];

        $this->actingAs($this->manager)->put(route('clients.update', $client), $values)
            ->assertRedirect(route('clients.show', $client))
            ->assertSessionHas('success', 'The client has been updated.');

        $this->assertDatabaseHas('clients', $values);
    }

    /** @test */
    public function a_user_without_the_delete_client_permission_cannot_delete_clients()
    {
        $client = Client::factory()->create();

        $this->actingAs($this->employee)->delete(route('clients.destroy', $client))
            ->assertStatus(403)
            ->assertSee('User does not have the right permissions.');

        $this->assertNotSoftDeleted($client);
    }

    /** @test */
    public function a_user_with_the_delete_client_permission_can_delete_clients()
    {
        $client = Client::factory()->create();

        $this->assertDatabaseHas('clients', ['id' => $client->id]);

        $this->actingAs($this->manager)->delete(route('clients.destroy', $client))
            ->assertRedirect(route('clients.index'))
            ->assertSessionHas('success', 'The client has been deleted.');

        $this->assertSoftDeleted($client);

        $this->get(route('clients.index'))->assertDontSee($client->company_name);
    }
    
    /** @test */
    public function when_a_client_gets_deleted_the_related_projects_get_deleted()
    {
        $client = Client::factory()->create();

        $project = Project::factory()->for($client)->create();

        $this->actingAs($this->manager)->delete(route('clients.destroy', $client))
            ->assertRedirect(route('clients.index'))
            ->assertSessionHas('success', 'The client has been deleted.');

        $this->assertSoftDeleted($client);
        $this->assertSoftDeleted($project);
    }

    /** @test */
    public function a_user_without_the_read_trashed_permission_cannot_see_clients_that_have_been_trashed()
    {
        $user = User::factory()->create();

        $this->actingAs($user)->get(route('clients.trashed'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');
    }

    /** @test */
    public function a_user_with_the_read_trashed_permission_can_see_clients_that_have_been_trashed()
    {
        $this->actingAs($this->admin)->get(route('clients.trashed'))
            ->assertOk()
            ->assertSee('No trashed clients yet.');

        $client = Client::factory()->create(['deleted_at' => now()]);
        
        $this->actingAs($this->admin)->get(route('clients.trashed'))
            ->assertOk()
            ->assertSeeText($this->stringLengthCap($client->company_name));
    }

    /** @test */
    public function only_a_user_with_the_restore_client_permission_can_see_the_restore_button_on_the_trashed_client_overview()
    {
        $user = User::factory()->create()->givePermissionTo('read_trashed');
        $client = Client::factory()->create(['deleted_at' => now()]);

        $this->actingAs($user)->get(route('clients.trashed'))
            ->assertSee($this->stringLengthCap($client->company_name))
            ->assertDontSee('Restore');

        $this->actingAs($this->admin)->get(route('clients.trashed'))
            ->assertSee($this->stringLengthCap($client->company_name))
            ->assertSee('Restore');
    }

    /** @test */
    public function only_a_user_with_the_restore_client_permission_can_restore_a_client()
    {
        $user = User::factory()->create();
        $client = Client::factory()->create(['deleted_at' => now()]);

        $this->actingAs($user)->patch(route('clients.restore', $client))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->assertSoftDeleted($client);

        $this->actingAs($this->admin)->patch(route('clients.restore', $client))
            ->assertRedirect(route('clients.trashed'))
            ->assertSessionHas('success', 'The client has been restored!');
        
        $this->assertNotSoftDeleted($client);

        $this->actingAs($this->admin)->get(route('clients.trashed'))
            ->assertDontSeeText($this->stringLengthCap($client->company_name));
    }

    /** @test */
    public function only_a_user_with_the_force_delete_client_permission_can_see_the_force_delete_button_on_the_trashed_client_overview()
    {
        $user = User::factory()->create()->givePermissionTo('read_trashed');
        $client = Client::factory()->create(['deleted_at' => now()]);

        $this->actingAs($user)->get(route('clients.trashed'))
            ->assertSeeText($this->stringLengthCap($client->company_name))
            ->assertDontSee('Force Delete');

        $this->actingAs($this->admin)->get(route('clients.trashed'))
            ->assertSeeText($this->stringLengthCap($client->company_name))
            ->assertSee('Force Delete');
    }

    /** @test */
    public function only_a_user_with_the_force_delete_permission_can_permanently_remove_a_client()
    {
        $user = User::factory()->create();
        $client = Client::factory()->create(['deleted_at' => now()]);

        $this->actingAs($user)->patch(route('clients.force-delete', $client))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->assertDatabaseHas('clients', ['id' => $client->id, 'company_name' => $client->company_name, 'deleted_at' => $client->deleted_at]);

        $this->actingAs($this->admin)->patch(route('clients.force-delete', $client))
            ->assertRedirect(route('clients.trashed'))
            ->assertSessionHas('success', 'The client has been permanently deleted.');
        
        $this->assertDatabaseMissing('clients', ['id' => $client->id, 'company_name' => $client->company_name]);
    }

    protected function stringLengthCap($string)
    {
        return strlen($string) > 35 ? Str::limit($string, 35) : $string;
    }

}
