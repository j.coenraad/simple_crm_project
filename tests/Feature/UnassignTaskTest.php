<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Database\Seeders\StatusSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Assert as PHPUnit;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

use function PHPUnit\Framework\assertTrue;

class UnassignTaskTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->setupPermissions();

        $this->employee = User::factory()->employee()->create();
        $this->manager = User::factory()->manager()->create();

        $this->seed(StatusSeeder::class);
    }

    public function setupPermissions()
    {
        Permission::findOrCreate('read_task');
        Permission::findOrCreate('unassign_task');

        Role::findOrCreate('employee')->givePermissionTo([
            'read_task'
        ]);

        Role::findOrCreate('manager')->givePermissionTo([
            'read_task', 'unassign_task'
        ]);
    }

    /** @test */
    public function a_user_cannot_see_the_unassign_button_when_a_task_is_not_assigned_to_a_user()
    {
        $task = Task::factory()->create(['user_id' => null]);

        $this->actingAs($this->employee)->get(route('tasks.show', $task))
            ->assertOk()
            ->assertDontSee('Unassign task');
    }

    /** @test */
    public function a_user_with_the_unassign_task_permission_cannot_unassign_a_task_that_is_not_assigned_to_a_user()
    {
        $task = Task::factory()->create(['user_id' => null]);
        $tmp = $task->updated_at;

        $this->actingAs($this->manager)->patch(route('tasks.unassign', $task), ['user_id' => null])
            ->assertRedirect(route('tasks.show', $task))
            ->assertSessionHasErrors(['error' => 'The task has already been unassigned.']);

        $this->assertDatabaseHas('tasks', ['id' => $task->id, 'updated_at' => $tmp]);
    }

    /** @test */
    public function a_user_can_only_see_the_unassign_task_button_when_a_task_is_assigned_to_themselves()
    {
        $task = Task::factory()->create(['user_id' => $this->employee->id]);
        $secondTask = Task::factory()->create(['user_id' => $this->manager->id]);

        $this->actingAs($this->employee)->get(route('tasks.show', $task))
            ->assertOk()
            ->assertSee('Unassign task');

        $this->actingAs($this->employee)->get(route('tasks.show', $secondTask))
            ->assertOk()
            ->assertDontSee('Unassign task');
    }

    /** @test */
    public function a_user_with_the_unassign_task_permission_can_see_the_unassign_task_button()
    {
        $task1 = Task::factory()->create(['user_id' => $this->employee->id]);
        $task2 = Task::factory()->create(['user_id' => $this->manager->id]);

        $this->actingAs($this->manager)->get(route('tasks.show', $task1))
            ->assertOk()
            ->assertSee('Unassign task');

        $this->actingAs($this->manager)->get(route('tasks.show', $task2))
            ->assertOk()
            ->assertSee('Unassign task');
    }

    /** @test */
    public function a_user_without_the_unassign_task_permission_can_only_unassign_their_own_task()
    {
        $task = Task::factory()->create(['user_id' => $this->employee->id]);
        $secondTask = Task::factory()->create(['user_id' => $this->manager->id]);

        $this->actingAs($this->employee)->patch(route('tasks.unassign', $task))
            ->assertRedirect(route('tasks.show', $task))
            ->assertSessionHas('success', 'The task has been unassigned.');

        $this->actingAs($this->employee)->patch(route('tasks.unassign', $secondTask))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');

        $this->assertDatabaseHas('tasks', ['id' => $task->id, 'user_id' => null]);
        $this->assertDatabaseHas('tasks', ['id' => $secondTask->id, 'user_id' => $this->manager->id]);
    }

    /** @test */
    public function a_user_with_the_unassign_task_permission_can_unassign_a_task()
    {
        $task = Task::factory()->create(['user_id' => $this->employee->id]);

        $this->actingAs($this->manager)->patch(route('tasks.unassign', $task))
            ->assertRedirect(route('tasks.show', $task))
            ->assertSessionHas('success', 'The task has been unassigned.');

        $this->assertDatabaseHas('tasks', ['id' => $task->id, 'user_id' => null]);
    }
}
