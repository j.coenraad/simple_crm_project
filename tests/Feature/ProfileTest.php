<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    public function setUp():void 
    {
        parent::setUp();

        Role::findOrCreate('employee');
    }

    /** @test */
    public function a_guest_cannot_visit_a_profile()
    {
        $user = User::factory()->employee()->create();

        $this->get(route('profiles.show', $user))
            ->assertRedirect(route('login'));
    }

    /** @test */
    public function a_user_without_a_changed_password_cannot_visit_a_profile()
    {
        $user = User::factory()->employee()->unchangedPassword()->create();

        $this->actingAs($user)->get(route('profiles.show', $user))
            ->assertRedirect(route('change-password.create', $user->token));
    }

    /** @test */
    public function a_user_cannot_visit_the_profile_of_a_deleted_user()
    {
        $user = User::factory()->employee()->create();
        $deletedUser = User::factory()->employee()->create(['deleted_at'=> now()]);

        $this->actingAs($user)->get(route('profiles.show', $deletedUser->id))
            ->assertNotFound();
    }

    /** @test */
    public function a_user_can_visit_their_own_profile()
    {
        $user = User::factory()->employee()->create();

        $this->actingAs($user)->get(route('profiles.show', $user))
        ->assertOk()
        ->assertSeeText([$user->name, $user->email, 'employee'])
        ->assertSee('/media/default/avatar.png')
        ->assertSee('Edit');
    }

    /** @test */
    public function a_user_can_visit_the_profile_of_another_user()
    {
        $user = User::factory()->employee()->create();
        $anotherUser = User::factory()->employee()->create();

        $this->actingAs($user)->get(route('profiles.show', $anotherUser->id))
            ->assertOk()
            ->assertSeeText([$anotherUser->name, $anotherUser->email, 'employee'])
            ->assertSee('/media/default/avatar.png')
            ->assertDontSee('Edit');
    }

    /** @test */
    public function a_user_can_only_see_the_change_password_form_on_their_own_profile()
    {
        $user = User::factory()->employee()->create();
        $anotherUser = User::factory()->employee()->create();

        $this->actingAs($user)->get(route('profiles.show', $user))
        ->assertOk()
        ->assertSee(['Change your password', 'Current password', 
            'New password', 'Confirm password', 'Change password']);

        $this->actingAs($user)->get(route('profiles.show', $anotherUser->id))
            ->assertOk()
            ->assertDontSee(['Change your password', 'Current password', 
            'New password', 'Confirm password', 'Change password']);
    }

    /** @test */
    public function a_user_can_only_upload_an_image_to_their_profile()
    {
        $user = User::factory()->employee()->create();

        Storage::fake('media');

        $document = UploadedFile::fake()->create('document.txt', 100, 'txt');

        $this->actingAs($user)->put(route('profiles.update', $user), ['image' => $document])
            ->assertSessionHasErrors([
                'image' => 'The image must be a file of type: png, jpg, jpeg.']);

        Storage::disk('media')->assertMissing('document.txt');

        $this->actingAs($user)->put(route('profiles.update', $user), ['name' => $user->name, 'image' => UploadedFile::fake()->image('avatar.jpg')])
            ->assertRedirect(route('profiles.show', $user))
            ->assertSessionHas('success', 'Your profile has been updated.');

        $this->actingAs($user)->get(route('profiles.show', $user))
            ->assertOk()
            ->assertSee($user->getFirstMedia()->getUrl());
    }

    /** @test */
    public function a_user_can_only_edit_their_own_profile()
    {
        $user = User::factory()->employee()->create();
        $anotherUser = User::factory()->employee()->create();

        $this->actingAs($user)->get(route('profiles.edit', $user))
            ->assertOk()
            ->assertSeeText($user->name);
        
        $values = [
            'name' => 'Changed Name'
        ];

        $this->actingAs($user)->put(route('profiles.update', $user), $values)
            ->assertRedirect(route('profiles.show', $user))
            ->assertSessionHas('success', 'Your profile has been updated.');

        $this->assertDatabaseHas('users', ['id' => $user->id, 'name' => 'Changed Name']);    

        $this->actingAs($user)->get(route('profiles.edit', $anotherUser))
            ->assertForbidden()
            ->assertSee('A user can only edit their own profile.');

        $this->actingAs($user)->put(route('profiles.update', $anotherUser), $values)
            ->assertForbidden()
            ->assertSee('A user can only edit their own profile.');

        $this->assertDatabaseMissing('users', ['id' => $anotherUser->id, 'name' => 'Changed Name']);
    }
}
