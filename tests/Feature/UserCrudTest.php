<?php

namespace Tests\Feature;

use App\Mail\AccountCreated;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class UserCrudTest extends TestCase
{
    use RefreshDatabase;

    protected $admin, $manager;

    public function setUp():void
    {
        parent::setUp();

        $this->setupPermissions();

        $this->admin = User::factory()->admin()->create();
        $this->manager = User::factory()->manager()->create();

        Mail::fake();
    }

    public function setupPermissions()
    {
        Permission::findOrCreate('create_user');
        Permission::findOrCreate('read_user');
        Permission::findOrCreate('edit_user');
        Permission::findOrCreate('delete_user');
        Permission::findOrCreate('restore_user');
        Permission::findOrCreate('read_trashed');

        Role::findOrCreate('super-admin');

        Role::findOrCreate('admin')->givePermissionTo([
            'create_user', 'read_user', 'edit_user', 'delete_user',
            'restore_user', 'read_trashed'
        ]);

        Role::findOrCreate('manager')->givePermissionTo('read_user');
        Role::findOrCreate('employee');

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    public function createSimpleUser()
    {
        return $user = User::factory()->create();
    }

    /** @test */
    public function a_guest_cannot_visit_the_user_management_index_page()
    {
        $this->get(route('users.index'))->assertRedirect(route('login'));
    }

    /** @test */
    public function a_user_without_the_read_user_permission_cannot_reach_the_users_index_page()
    {
        $this->actingAs($this->createSimpleUser())->get(route('users.index'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');
    }

    /** @test */
    public function a_user_with_the_read_user_permission_can_reach_the_user_management_index_page()
    {
        $employee = User::factory()->employee()->create();
        $manager = User::factory()->manager()->create();

        $this->actingAs($this->admin)->get(route('users.index'))
            ->assertOk()
            ->assertSeeText([$this->admin->name, $this->admin->email, 'admin'])
            ->assertSeeText([$manager->name, $manager->email, 'manager'])
            ->assertSeeText([$employee->name, $employee->email, 'employee']);  
    }

    /** @test */
    public function only_the_super_admin_can_see_their_account_on_the_user_management_index_page()
    {
        $superAdmin = User::factory()->superAdmin()->create();
        
        $this->actingAs($superAdmin)->get(route('users.index'))
            ->assertOk()
            ->assertSeeText([$superAdmin->name, $superAdmin->email]);

        $this->actingAs($this->admin)->get(route('users.index'))
            ->assertOk()
            ->assertDontSeeText([$superAdmin->name, $superAdmin->email]);
    }

    /** @test */
    public function a_user_can_search_for_user_by_name_or_email()
    {
        $this->withoutExceptionHandling();
        $user1 = User::factory()->create(['name' => 'John Doe', 'email' => 'john@example.com']);
        $user2 = User::factory()->create(['name' => 'Jane Doe', 'email' => 'jane@example.com']);
        $user3 = User::factory()->create(['name' => 'Taylor Otwell', 'email' => 'taylor@example.com']);

        $this->actingAs($this->admin)->get(route('users.index', ['search' => 'Doe']))
            ->assertOk()
            ->assertSeeText([
                $user1->name,
                $user2->name
            ])
            ->assertDontSeeText([
                $user3->name,
            ]);

        $this->actingAs($this->admin)->get(route('users.index', ['search' => 'taylor@example.com']))
            ->assertOk()
            ->assertSeeText([$user3->name, $user3->email])
            ->assertDontSeeText([
                $user1->name, $user1->email,
                $user2->name, $user2->email    
            ]);
    }

    /** @test */
    public function a_user_without_the_create_user_permission_cannot_create_a_new_user()
    {
        $this->actingAs($this->createSimpleUser())->get(route('users.create'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->actingAs($this->createSimpleUser())->post(route('users.store'),[
            'name' => 'Sample User',
            'emai' => 'sample.user@example.com'
        ])->assertForbidden();

        Mail::assertNothingQueued(AccountCreated::class);

        $this->assertDatabaseMissing('users', [
            'name' => 'Sample User',
            'email' => 'sample.user@example.com'
        ]);
    }

    /** @test */
    public function a_user_with_the_create_user_permission_can_create_a_new_user()
    {
        $this->actingAs($this->admin)->get(route('users.create'))
            ->assertOk();

        $this->actingAs($this->admin)->post(route('users.store'), [
            'name' => 'Sample User',
            'email' => 'sample.user@example.com'
        ])
            ->assertRedirect(route('users.index'))
            ->assertSessionHas('success', 'A new user account has been created.');

        $this->assertDatabaseHas('users', [
            'name' => 'Sample User',
            'email' => 'sample.user@example.com'
        ]);
    }

    /** @test */
    public function an_email_is_sent_when_a_new_user_has_been_created()
    {
        $this->withoutExceptionHandling();
        $values = [
            'name' => 'Elric Doe',
            'email' => 'elric.doe@example.com',
            'role_id' => 4 // employee
        ];

        $this->actingAs($this->admin)->post(route('users.store'), $values)
            ->assertRedirect(route('users.index'))
            ->assertSessionHas('success', 'A new user account has been created.');

        Mail::assertQueued(AccountCreated::class);
    }

    /** @test */
    public function the_email_of_user_must_be_a_valid_email()
    {
        $values = [
            'name' => 'Some User',
            'email' => 'some.user-example.com'
        ];

        $this->actingAs($this->admin)->post(route('users.store'), $values)
            ->assertSessionHasErrors([
                'email' => 'The email must be a valid email address.'
            ]);

        $this->assertDatabaseMissing('users', $values);
    }

    /** @test */
    public function the_email_of_a_user_must_be_unique()
    {
        $user = User::factory()->create(['email' => 'some.user@example.com']);

        $values = [
            'name' => 'New User',
            'email' => 'some.user@example.com',
        ];

        $this->actingAs($this->admin)->post(route('users.store'), $values)
            ->assertSessionHasErrors([
                'email' => 'The email has already been taken.'
            ]);

        $this->assertDatabaseMissing('users', $values);
    }

    /** @test */
    public function a_user_must_be_assigned_a_valid_role()
    {
        $values = [
            'name' => 'New User',
            'email' => 'new.user@example.com',
            'role_id' => 99
        ];

        $this->actingAs($this->admin)->post(route('users.store'), $values)
            ->assertSessionHasErrors([
                'role_id' => 'The selected role is invalid.'
            ]);
        
        $this->assertDatabaseMissing('users', $values);
    }

    /** @test */
    public function a_user_without_the_edit_user_permission_cannot_edit_a_user()
    {
        $secondaryUser = User::factory()->employee()->create();
        $manager = Role::findByName('manager');

        $values = [
            'name' => 'New User',
            'email' => 'new.user@example.com',
            'role_id' => $manager->id
        ];

        $this->actingAs($this->createSimpleUser())->get(route('users.edit', $secondaryUser))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');
        
        $this->actingAs($this->createSimpleUser())->put(route('users.update', $secondaryUser), $values)
            ->assertForbidden();

        $this->assertNotContains($values, $secondaryUser->fresh()->toArray());
        $this->assertFalse($secondaryUser->fresh()->hasRole($manager->id));
        $this->assertTrue($secondaryUser->fresh()->hasRole('employee'));

        $this->assertDatabaseMissing('users', [
            'name' => 'New User', 
            'email' => 'new.user@example.com'
        ]);
    }

    /** @test */
    public function a_user_with_the_edit_user_permission_can_edit_a_user()
    {
        $user = $this->createSimpleUser()->assignRole('employee');
        $manager = Role::findByName('manager');

        $this->actingAs($this->admin)->get(route('users.edit', $user))
            ->assertOk()
            ->assertSee([$user->name, $user->email]);

        $values = [
            'name' => 'New User',
            'email' => 'new.user@example.com',
            'role_id' => $manager->id
        ];

        $this->actingAs($this->admin)->put(route('users.update', $user), $values)
            ->assertRedirect(route('users.index'))
            ->assertSessionHas('success', 'The user account has been updated.');


        $this->assertTrue($user->fresh()->hasRole($manager->id));
        $this->assertFalse($user->fresh()->hasRole('employee'));

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => 'New User', 
            'email' => 'new.user@example.com'
        ]);
    }

    /** @test */
    public function a_user_without_the_delete_user_permission_cannot_delete_a_user()
    {
        $secondaryUser = User::factory()->create();

        $this->actingAs($this->createSimpleUser())->delete(route('users.destroy', $secondaryUser))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->assertNotSoftDeleted($secondaryUser);
    }

    /** @test */
    public function a_user_with_the_delete_user_permission_can_delete_a_user()
    {
        $user = User::factory()->create();

        $this->actingAs($this->admin)->delete(route('users.destroy', $user))
            ->assertRedirect(route('users.index'))
            ->assertSessionHas('success', 'The user account has been deleted.');

        $this->assertSoftDeleted($user);
    }

    /** @test */
    public function when_a_user_is_deleted_all_assigned_projects_and_tasks_get_unassigned_from_this_user()
    {
        $manager = User::factory()->manager()->create();
        $project = Project::factory()->create(['manager_id' => $manager->id]);
        $task = Task::factory()->for($manager)->create();

        $this->actingAs($this->admin)->delete(route('users.destroy', $manager))
            ->assertRedirect(route('users.index'))
            ->assertSessionHas('success', 'The user account has been deleted.');

        $this->assertSoftDeleted($manager);

        $this->assertDatabaseHas('projects', [
            'id' => $project->id,
            'manager_id' => null
        ]);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'user_id' => null
        ]);
    }

    /** @test */
    public function only_a_user_with_the_read_trashed_permission_can_reach_the_trashed_users_overview_page()
    {
        $account = User::factory()->create(['deleted_at' => now()]);

        $this->actingAs($this->createSimpleUser())->get(route('users.trashed'))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->actingAs($this->admin)->get(route('users.trashed'))
            ->assertOk()
            ->assertSeeText($account->name);
    }

    /** @test */
    public function only_a_user_with_the_restore_user_permission_can_see_the_restore_button_on_the_trashed_users_overview()
    {
        $user = $this->createSimpleUser()->givePermissionTo('read_trashed');
        $account = User::factory()->create(['deleted_at' => now()]);

        $this->actingAs($user)->get(route('users.trashed'))
            ->assertOk()
            ->assertSeeText($account->name)
            ->assertDontSee('Restore');

        $this->actingAs($this->admin)->get(route('users.trashed'))
            ->assertOk()
            ->assertSeeText($account->name)
            ->assertSee('Restore');
    }

    /** @test */
    public function only_a_user_with_the_restore_user_permission_can_restore_a_user()
    {
        $account = User::factory()->create(['deleted_at' => now()]);
        $this->actingAs($this->createSimpleUser())->patch(route('users.restore', $account))
            ->assertForbidden()
            ->assertSee('User does not have the right permissions.');

        $this->actingAs($this->admin)->patch(route('users.restore', $account))
            ->assertRedirect(route('users.trashed'))
            ->assertSessionHas('success', 'The user has been restored.');
    }

    /** @test */
    public function only_accounts_that_have_been_trashed_for_at_least_2_months_will_be_removed_by_the_remove_users_artisan_command()
    {

        $deletedUser = User::factory()->create([
            'created_at' => now()->subMonths(6), 
            'deleted_at' => now()->subMonths(3)
        ]);

        $recentlyDeleted = User::factory()->create([
            'created_at' => now()->subMonths(4),
            'deleted_at' => now()->subMonth()
        ]);

        
        $this->artisan('remove:users')
        ->expectsOutput('Removed 1 user(s) that were discarded at least 2 months ago.')
        ->assertSuccessful();
        
        $this->assertDatabaseMissing('users', $deletedUser->toArray());
        
        $this->assertSoftDeleted($recentlyDeleted);
    }
}
