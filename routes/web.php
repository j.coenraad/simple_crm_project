<?php

use App\Http\Controllers\AssignTaskController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GrantNewTokenController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\RequestTokenController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TrashedClientController;
use App\Http\Controllers\TrashedProjectController;
use App\Http\Controllers\TrashedTaskController;
use App\Http\Controllers\TrashedUserController;
use App\Http\Controllers\UnassignTaskController;
use App\Http\Controllers\UpdatePasswordController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Profiler\Profile;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::redirect('/', 'login');

Route::middleware(['guest'])->group(function() {
    Route::controller(ChangePasswordController::class)->prefix('/change-password')->group(function () {
        Route::get('/{token}', 'create')->name('change-password.create');
        Route::post('/', 'store')->name('change-password.store');
    });

    Route::controller(RequestTokenController::class)->prefix('/request-token')->group(function () {
        Route::get('/{token}', 'create')->name('request-token.create');
        Route::post('/', 'store')->name('request-token.store');
    });
});


Route::middleware(['auth', 'password_not_changed'])->group(function (){
    Route::get('/dashboard', DashboardController::class)->name('dashboard');
    Route::post('/new_password', [UpdatePasswordController::class, 'store'])->name('new-password.store');

    Route::controller(ProfileController::class)->prefix('/profiles/{user}')->group(function (){
        Route::get('/', 'show')->name('profiles.show');
        Route::get('/edit', 'edit')->name('profiles.edit');
        Route::put('/', 'update')->name('profiles.update');
    });

    Route::resource('users', UserController::class);
    Route::patch('/users/{user}/restore', [UserController::class, 'restore'])->withTrashed()->name('users.restore');
    Route::post('users/{user}/token', GrantNewTokenController::class)->name('users.new-token');

    Route::resource('clients', ClientController::class);
    Route::patch('/clients/{client}/restore', [ClientController::class, 'restore'])->withTrashed()->name('clients.restore');
    Route::patch('/clients/{client}/forcedelete', [ClientController::class, 'forceDelete'])->withTrashed()->name('clients.force-delete');
    
    Route::resource('projects', ProjectController::class);
    Route::patch('/projects/{project}/restore', [ProjectController::class, 'restore'])->withTrashed()->name('projects.restore');
    Route::patch('/projects/{project}/forcedelete', [ProjectController::class, 'forceDelete'])->withTrashed()->name('projects.force-delete');

    Route::resource('tasks', TaskController::class);
    Route::patch('/tasks/{task}/restore', [TaskController::class, 'restore'])->withTrashed()->name('tasks.restore');
    Route::patch('/tasks/{task}/forcedelete', [TaskController::class, 'forceDelete'])->withTrashed()->name('tasks.force-delete');
    Route::patch('/tasks/{task}/unassign', UnassignTaskController::class)->name('tasks.unassign');
    Route::patch('/tasks/{task}/assign', AssignTaskController::class)->name('tasks.self-assign');

    Route::get('/trashed/projects', TrashedProjectController::class)->name('projects.trashed');
    Route::get('/trashed/clients', TrashedClientController::class)->name('clients.trashed');
    Route::get('/trashed/tasks', TrashedTaskController::class)->name('tasks.trashed');
    Route::get('/trashed/users', TrashedUserController::class)->name('users.trashed');
});

